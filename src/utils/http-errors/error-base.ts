import { RestErrorMessage } from '@src/interfaces';

export class HttpError {
  public errorCode: string;
  public errorDescription: string;
  public statusCode: number;
  public messageType = 'error';
  public obj: RestErrorMessage;

  constructor(statusCode: number, errorCode: string, errorDescription: string) {
    this.errorCode = errorCode;
    this.errorDescription = errorDescription;
    this.statusCode = statusCode;
  }

  public toObj(): RestErrorMessage {
    return {
      type: this.messageType,
      code: this.errorCode,
      description: this.errorDescription
    };
  }
}
