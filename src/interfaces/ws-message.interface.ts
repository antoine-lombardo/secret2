export interface WSMessage {
    type: string;
    value: string;
}

export default WSMessage;
