
import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpDelete, httpGet, httpPatch, httpPost, request, requestParam } from 'inversify-express-utils';
import { AlbumsService, CollabService, DrawingsService, JoinRequestsService, NotificationsService, SocketsService } from '@src/services';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import * as HttpErrors from '@src/utils/http-errors';
import { Drawing } from '@src/interfaces/drawing.interface';
import { AlbumMemberPermissionError, AlbumNotFoundError } from '@src/utils/http-errors';
import { SocketMessage } from '@src/interfaces';

@controller('/albums')
export class AlbumsController extends BaseHttpController {



  @inject(AlbumsService) public albumsService: AlbumsService;
  @inject(CollabService) public collabService: CollabService;
  @inject(DrawingsService) public drawingsService: DrawingsService;
  @inject(JoinRequestsService) public joinRequestsService: JoinRequestsService;
  @inject(NotificationsService) public notificationsService: NotificationsService;
  @inject(SocketsService) public socketsService: SocketsService;



  @httpGet('/', AuthMiddleware)
  async getAll(@request() req: Request) {
    const albums = await this.albumsService.findAll(this.httpContext.user.details.id, req);
    if (!albums) { throw new HttpErrors.DatabaseError(); }
    return {
      type: 'success',
      albums
    };
  }



  @httpGet('/public', AuthMiddleware)
  async getPublic(@request() req: Request) {
    const album = await this.albumsService.findPublic(req);
    if (!album) { throw new HttpErrors.DatabaseError(); }
    return {
      type: 'success',
      album
    };
  }



  @httpGet('/mine', AuthMiddleware)
  async getMine(@request() req: Request) {
    const albums = await this.albumsService.findFromUser(this.httpContext.user.details.id, this.httpContext.user.details.id, req);
    if (!albums) { throw new HttpErrors.DatabaseError(); }
    return {
      type: 'success',
      albums
    };

  }



  @httpPost('/', AuthMiddleware)
  async create(@request() req: Request) {
    const name: string = req.body.name;
    const description: string = req.body.description;

    // Missing parameters
    if (!name) { throw new HttpErrors.MissingAlbumNameError(); }
    if (!description) { throw new HttpErrors.MissingAlbumDescriptionError(); }

    // Try to create album
    const album = await this.albumsService.create(this.httpContext.user.details.id, name, description, req);
    if (!album) { throw new HttpErrors.DatabaseError(); }

    return {
      type: 'success',
      description: 'Album créé.',
      album
    };

  }



  @httpPatch('/:id', AuthMiddleware)
  async modify(@requestParam('id') id: string, @request() req: Request) {
    const name: string = req.body.name;
    const description: string = req.body.description;

    // Missing parameters
    if (!name) { throw new HttpErrors.MissingAlbumNameError(); }
    if (!description) { throw new HttpErrors.MissingAlbumDescriptionError(); }

    // Try to modify the album
    const album = await this.albumsService.modify(this.httpContext.user.details.id, id, name, description, req);
    if (!album) { throw new HttpErrors.DatabaseError(); }

    return {
      type: 'success',
      description: 'Album modifié.',
      album
    };

  }



  @httpDelete('/:id', AuthMiddleware)
  async delete(@requestParam('id') id: string, @request() req: Request) {
    // Force all active collaborators to quit
    const drawings = await this.drawingsService.findInAlbum(id, this.httpContext.user.details.id, req);
    for (const drawing of drawings) {
      await this.collabService.handleDrawingDeleted(drawing.id, 'album_deleted', 'Cet album vient d\'être supprimé.');
    }

    await this.albumsService.delete(this.httpContext.user.details.id, id);
    await this.drawingsService.deleteFromAlbum(id);
    this.socketsService.emitToAllExcept({
      type: 'generalEvent',
      eventType: 'album_deleted',
      data: id
    } as SocketMessage, this.httpContext.user.details);
    return {
      type: 'success',
      description: 'Album supprimé.'
    };
  }


  @httpGet('/:id', AuthMiddleware)
  async getInfos(@requestParam('id') id: string, @request() req: Request) {
    const album = await this.albumsService.findById(id, this.httpContext.user.details.id, req);
    if (!album) { throw new AlbumNotFoundError(); }
    return {
      type: 'success',
      album
    };
  }



  @httpGet('/:id/drawings', AuthMiddleware)
  async getDrawings(@requestParam('id') id: string, @request() req: Request) {
    const album = await this.albumsService.findById(id, this.httpContext.user.details.id, req);
    if (!album) { throw new AlbumNotFoundError(); }
    if (!album.hasAccess) { throw new AlbumMemberPermissionError(); }
    const drawings = await this.drawingsService.findInAlbum(id, this.httpContext.user.details.id, req);
    if (!drawings) { throw new HttpErrors.DatabaseError(); }
    return {
      type: 'success',
      drawings: drawings
    };
  }



  @httpGet('/:id/exposition', AuthMiddleware)
  async getExposition(@requestParam('id') id: string, @request() req: Request) {
    const drawings = await this.drawingsService.findExposed(id, this.httpContext.user.details.id, req);
    if (!drawings) { throw new HttpErrors.DatabaseError(); }
    const exposition = [];
    for (const drawing of drawings) {
      exposition.push({
        title: drawing.title,
        author: drawing.author,
        dateCreated: drawing.dateCreated,
        thumbnail: drawing.thumbnail,
        original: drawing.original
      });
    }
    exposition.sort((a: Drawing, b: Drawing) => {
      if (a.dateCreated < b.dateCreated) {
          return -1;
      }
      if (a.dateCreated > b.dateCreated) {
          return 1;
      }
      return 0;
    });
    exposition.reverse();
    return {
      type: 'success',
      exposition
    };
  }



  @httpPost('/:id/join', AuthMiddleware)
  async askToJoin(@requestParam('id') id: string, @request() req: Request) {
    // Get album details
    const album = await this.albumsService.findById(id, this.httpContext.user.details.id, req);

    // Create a join request
    const joinRequest = await this.joinRequestsService.create(this.httpContext.user.details.id, id, req);
    if (!joinRequest) { throw new HttpErrors.DatabaseError(); }

    // Send a notification to every member
    for (const member of album.members) {
      this.notificationsService.send(
        member.id,
        'ask_access',
        'Demande d\'accès',
        `Vous avez reçu une demande d\'accès pour l\'album "${album.name}"`,
        'album',
        id
      );
    }

    // Send response
    return {
      type: 'success',
      joinRequest
    };
  }



  @httpGet('/:id/quit', AuthMiddleware)
  async quit(@requestParam('id') id: string, @request() req: Request) {
    const album = await this.albumsService.findById(id, this.httpContext.user.details.id, req);
    // Check if album exists
    if (!album) { throw new HttpErrors.AlbumNotFoundError(); }
    // Check if user is the creator
    if (album.creator.id === this.httpContext.user.details.id) { throw new HttpErrors.AlbumCreatorCannotQuitError(); }
    // Check if user is a member
    let found = false;
    for (const member of album.members) { if (member.id === this.httpContext.user.details.id) { found = true; } }
    if (!found) { throw new HttpErrors.AlbumMemberPermissionError(); }
    // Quit album
    await this.albumsService.removeMember(id, this.httpContext.user.details.id);
    await this.drawingsService.fixDrawings(id, this.httpContext.user.details.id, album.creator.id);
    return {
      type: 'success',
      description: 'Album quitté avec succès.'
    };
  }



  @httpGet('/:id/requests', AuthMiddleware)
  async getRequests(@requestParam('id') id: string, @request() req: Request) {
    const joinRequests = await this.joinRequestsService.findPendingForAlbum(id, this.httpContext.user.details.id, req);
    if (!joinRequests) { throw new HttpErrors.DatabaseError(); }
    return {
      type: 'success',
      joinRequests
    };
  }



}
