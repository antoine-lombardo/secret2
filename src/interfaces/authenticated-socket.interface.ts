import { Socket } from 'socket.io';
import { User } from '.';

export interface AuthenticatedSocket extends Socket {
    authFailed: boolean;
    user: User;
}
