import { LayerStore } from '@src/interfaces';
import mongoose from 'mongoose';

export const LayerModel = mongoose.model<LayerStore>(
    'Layer',
    new mongoose.Schema({
        type: String,
        creator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        valid: Boolean,
        strokeColor: String,
        fillColor: String,
        strokeThickness: Number,
        isFilled: Boolean,
        isStroked: Boolean,
        drawing: {type: mongoose.Schema.Types.ObjectId, ref: 'Drawing'},
        timestamp: Number,
        path: [{
            x : Number,
            y : Number
        }]
    })
);
