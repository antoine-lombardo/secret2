import { Response, Request } from 'express';
import { BaseHttpController, controller, httpGet, request, requestParam, response } from 'inversify-express-utils';
import * as fs from 'fs';
import * as path from 'path';
import { config } from '@src/config';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { formatHostname } from '@src/utils/rest-utils';
import * as HttpErrors from '@src/utils/http-errors';

const AVATARS_DIR = path.normalize(config.FilesDir + '/avatars');

@controller('/avatars')
export class AvatarsController extends BaseHttpController {

  @httpGet('/:id/:filename', AuthMiddleware)
  async getAvatar(@requestParam('id') id: string, @requestParam('filename') filename: string, @response() res: Response) {
    const filePath = path.normalize(AVATARS_DIR + '/' + id + '/' + filename);
    if (!fs.existsSync(filePath)) { throw new HttpErrors.AvatarNotFoundError(); }
    return res.download(filePath, filename, (err: any) => {
          console.log(err);
       });
  }


  @httpGet('/preset')
  async getPreset(@request() req: Request) {
    const avatarDirs = fs.readdirSync(`${AVATARS_DIR}/base`);
    const avatars = [];
    for (const avatarDir of avatarDirs) {
      if (fs.existsSync(`${AVATARS_DIR}/base/${avatarDir}/original.png`) && fs.existsSync(`${AVATARS_DIR}/base/${avatarDir}/resized.png`)) {
        avatars.push({
          name: avatarDir,
          original: `${formatHostname(req)}/files/avatars/base/${avatarDir}/original.png`,
          resized: `${formatHostname(req)}/files/avatars/base/${avatarDir}/resized.png`,
        });
      }
    }
    return {
      type: 'success',
      avatars
    };
  }

}
