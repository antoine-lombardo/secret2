import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

export class DatabaseError extends HttpError {
    constructor() {
        super(StatusCodes.INTERNAL_SERVER_ERROR, 'DATABASE_ERROR', 'Erreur de connexion avec la base de données.');
    }
}
