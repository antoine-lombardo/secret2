import * as SocketIO from 'socket.io';
import * as Express from 'express';
import * as http from 'http';
import * as RestUtils from '@src/utils/rest-utils';
import { inject, injectable, LazyServiceIdentifer } from 'inversify';
import { interfaces } from 'inversify-express-utils';
import { InversifyExpressServer } from 'inversify-express-utils';
import { container } from '@src/inversify.config';
import { config } from '@src/config';
import { AuthenticatedSocket } from '@src/interfaces';
import { AuthenticationService, UsersService, SocketsService, DatabaseService } from '@src/services';
import { logger } from '@services/logger';
import { AuthWSMiddleware, BaseWSMiddleware } from '@src/middleware';
import * as fs from 'fs';
import * as path from 'path';



@injectable()
export class ServerService {
    private server: http.Server;

    // Express
    private expressApp: Express.Application;

    // Socket
    private socketApp: SocketIO.Server;
    private socketMiddlewares: BaseWSMiddleware[];

    // Services
    private socketsService: SocketsService;
    private databaseService: DatabaseService;


    constructor(
        @inject(new LazyServiceIdentifer(() => SocketsService)) socketsService: SocketsService,
        @inject(new LazyServiceIdentifer(() => DatabaseService)) databaseService: DatabaseService,
        @inject(new LazyServiceIdentifer(() => AuthWSMiddleware)) authWSMiddleware: AuthWSMiddleware
    ) {
        // Directories
        this.initializeDirectories();
        // Services
        this.socketsService = socketsService;
        this.databaseService = databaseService;
        // Websocket middlwares
        this.socketMiddlewares = [authWSMiddleware];
        // Initialization
        this.databaseService.connect(() => {
            this.initializeExpress();
            this.initializeSocket();
            this.listen();
        });
    }

    private listen(): void {
        this.server.listen(config.Port, () => {
            logger.info(`Server listening on port ${config.Port}`);
        });
    }

    private initializeDirectories() {
        // List directories
        const filesDir = path.normalize(config.FilesDir);
        const avatarsDir = filesDir + '/avatars';
        const baseAvatarsDir = avatarsDir + '/base';
        const drawingsDir = filesDir + '/drawings';
        const activeDrawingsDir = drawingsDir + '/active';
        const doneDrawingsDir = drawingsDir + '/done';
        // Create directories
        if (!fs.existsSync(filesDir)) { fs.mkdirSync(filesDir); }
        if (!fs.existsSync(avatarsDir)) { fs.mkdirSync(avatarsDir); }
        if (!fs.existsSync(baseAvatarsDir)) { fs.mkdirSync(baseAvatarsDir); }
        if (!fs.existsSync(drawingsDir)) { fs.mkdirSync(drawingsDir); }
        if (!fs.existsSync(activeDrawingsDir)) { fs.mkdirSync(activeDrawingsDir); }
        if (!fs.existsSync(doneDrawingsDir)) { fs.mkdirSync(doneDrawingsDir); }
    }

    // --------------------------------------------------- //
    //                   EXPRESS SERVER                    //
    // --------------------------------------------------- //

    private initializeExpress() {
        const server = new InversifyExpressServer(container, null, null, null, AuthProvider);

        server.setConfig((app) => {
            app.use(Express.urlencoded({ limit: '50mb', extended: true }));
            app.use(Express.json({limit: '50mb'}));
            app.use('/files', Express.static(config.FilesDir));
        });
        server.setErrorConfig((app) => {
            app.use((error: any, request: Express.Request, response: Express.Response, next: () => void) => {
                const ipPrefix = RestUtils.formatIpPrefix(request);
                try {
                    if (!error.errorCode || !error.statusCode || !error.toObj) { throw new Error(); }
                    logger.error(`[REST] ${ipPrefix}${error.statusCode} - ${error.errorCode}`);
                    response.status(error.statusCode).send(error.toObj());
                } catch {
                    logger.error(`[REST] ${ipPrefix}500 - Unexpected error.`);
                    logger.error(error.stack);
                    response.status(500).send({
                        type: 'error',
                        message: error.message
                    });
                }
            });
        });
        this.expressApp = server.build();
        this.server = http.createServer(this.expressApp);
        logger.info('Express server initialized.');
    }


    // --------------------------------------------------- //
    //                   SOCKET SERVER                     //
    // --------------------------------------------------- //

    private initializeSocket() {
        // Initialize the socket server
        this.socketApp = new SocketIO.Server(this.server, {
            perMessageDeflate: false
        });
        // Register middlewares
        for (const middleware of this.socketMiddlewares) {
            this.socketApp.use(
                (socket: AuthenticatedSocket, next: (err?: Error) => void) => {
                    middleware.middleware(socket, next);
                }
            );
        }
        // Register socket app
        this.socketsService.registerApp(this.socketApp);
        // Register the socket handler
        this.socketApp.on('connection',
            (socket: AuthenticatedSocket) => {
                this.socketsService.socketHandler(socket);
            }
        );
        logger.info('Socket.IO server initialized.');
    }
}





// --------------------------------------------------- //
//              AUTH PROVIDER (EXPRESS)                //
// --------------------------------------------------- //


class Principal implements interfaces.Principal {
    public details: any;
    public constructor(details: any) {
        this.details = details;
    }
    public async isAuthenticated(): Promise<boolean> {
        if (this.details) { return true; }
        else { return false; }
    }
    public isResourceOwner(resourceId: any): Promise<boolean> {
        return Promise.resolve(resourceId === 1111);
    }
    public isInRole(role: string): Promise<boolean> {
        return Promise.resolve(role === 'admin');
    }
}



@injectable()
class AuthProvider implements interfaces.AuthProvider {

    @inject(AuthenticationService) public authenticationService: AuthenticationService;
    @inject(UsersService) public usersService: UsersService;

    public async getUser(
        req: Express.Request,
        res: Express.Response,
        next: Express.NextFunction
    ): Promise<interfaces.Principal> {
        const token = (req.headers['authorization'] as string)?.replace('Bearer ', '');
        if (!token) { return new Principal(null); }
        const payload = this.authenticationService.validateToken(token);
        if (!payload) { return new Principal(null); }
        const user = await this.usersService.findUserById(payload.id);
        if (!user) { return new Principal(null); }
        return new Principal(user);
    }

}
