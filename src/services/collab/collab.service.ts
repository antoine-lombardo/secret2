import { logger } from '@services/logger';
import { AuthenticatedSocket, EditMessage, SocketMessage } from '@src/interfaces';
import { inject, injectable, LazyServiceIdentifer } from 'inversify';
import { SocketsService } from '@src/services';
import { DrawingsService, LayersService, UsersService } from '../database';
import { LayerCreatedMessage } from '@src/interfaces/socket-responses/layer-created-message..interface';
import { DrawingSession } from '@src/classes/drawing-session';

@injectable()
export class CollabService {
    private DrawingSessionConstructor: { new(drawingId: string): DrawingSession };
    private socketsService: SocketsService;
    private layersService: LayersService;
    private drawingsService: DrawingsService;
    private usersService: UsersService;
    private drawingSessions: {[drawingId: string]: DrawingSession} = {};
    private activeCollaborators: {[userId: string]: string} = {};

    constructor(
        @inject(new LazyServiceIdentifer(() => SocketsService)) socketsService: SocketsService,
        @inject(new LazyServiceIdentifer(() => LayersService)) layersService: LayersService,
        @inject(new LazyServiceIdentifer(() => DrawingsService)) drawingsService: DrawingsService,
        @inject(new LazyServiceIdentifer(() => UsersService)) usersService: UsersService,
        @inject(new LazyServiceIdentifer(() => 'DrawingSessionConstructor')) DrawingSessionConstructor: { new(): DrawingSession }
    ) {
        this.socketsService = socketsService;
        this.layersService = layersService;
        this.drawingsService = drawingsService;
        this.usersService = usersService;
        this.DrawingSessionConstructor = DrawingSessionConstructor;

        // Register socket handler
        this.socketsService.registerHandler('edit',
            (socket: AuthenticatedSocket, message: EditMessage, callback: (response: SocketMessage) => void) => {
                this.receiveMessage(socket, message, callback);
            }
        );

        // TEMP: Register on disconnect
        this.socketsService.registerOnDisconnect((socket: AuthenticatedSocket) => {
            this.handleDisconnect(socket);
        });

        // TEMP: Register on connect (auto drawing session)
        // this.socketsService.registerOnConnect((socket: AuthenticatedSocket) => {
        //    this.handleJoin(socket, 'default');
        // });
    }


    //
    // Getters
    //

    public getLayers(drawingId: string) {
        if (!this.drawingSessions[drawingId]) { return []; }
        const layers: EditMessage[] = [];
        for (const layer of Object.values(this.drawingSessions[drawingId].layers)) {
            if (layer.editType !== 'delete') { layers.push(layer); }
        }
        return layers;
    }



    //
    // Handle clients
    //

    public async handleJoin(socket: AuthenticatedSocket, drawingId: string) {
        if (this.activeCollaborators[socket.user.id] === drawingId) { return; }
        this.handleQuit(socket);
        const drawingSession = await this.loadDrawingSession(drawingId);
        this.activeCollaborators[socket.user.id] = drawingId;
        this.drawingsService.addCollaborator(socket.user.id, drawingId);
        drawingSession.handleJoin(socket);
    }

    public handleQuit(socket: AuthenticatedSocket): string {
        // Do nothing if user is not active in any drawing session
        if (!this.activeCollaborators[socket.user.id]) { return; }
        // Remove user from its current drawing session
        const drawingSession = this.getUserDrawingSession(socket);
        if (drawingSession) { drawingSession.handleQuit(socket); }
        delete this.activeCollaborators[socket.user.id];
        this.drawingsService.removeCollaborator(socket.user.id);
        // Keep drawing id
        const savedDrawingId = drawingSession.drawingId;
        // Unload drawing session if not used anymore
        for (const drawingId of Object.values(this.activeCollaborators)) {
            if (drawingId === drawingSession.drawingId) { return savedDrawingId; }
        }
        this.unloadDrawingSession(drawingSession.drawingId);
        return savedDrawingId;
    }

    public handleDisconnect(socket: AuthenticatedSocket) {
        if (!socket.user) { return; }
        this.handleQuit(socket);
    }

    public getActiveUsers(drawingId: string): string[] {
        if (!this.drawingSessions[drawingId]) { return []; }
        return Object.keys(this.drawingSessions[drawingId].editors);
    }


    //
    // Handle special events
    //

    public async handleDrawingDeleted(drawingId: string, reason: string, message: string) {
        // Do nothing if drawing session isn't loaded
        if (!this.drawingSessions[drawingId]) { return; }
        // Force every active collaborator to quit
        await this.drawingSessions[drawingId].handleDrawingDeleted(reason, message);
        logger.error(`Forcefully unloaded drawing "${drawingId}" from memory.`);
        // Delete from memory
        delete this.drawingSessions[drawingId];
        // Delete all active user linked to the drawing session
        for (const userId of Object.keys(this.activeCollaborators)) {
            if (this.activeCollaborators[userId] === drawingId) {
                delete this.activeCollaborators[userId];
            }
        }
    }



    //
    // Handle drawing sessions
    //

    public async loadDrawingSession(drawingId: string): Promise<DrawingSession> {
        // Do nothing if drawing session is already loaded
        if (this.drawingSessions[drawingId]) { return this.drawingSessions[drawingId]; }
        // Load drawing session
        this.drawingSessions[drawingId] = new this.DrawingSessionConstructor(drawingId);
        this.drawingSessions[drawingId].layersService  = this.layersService;
        this.drawingSessions[drawingId].socketsService = this.socketsService;
        this.drawingSessions[drawingId].usersService   = this.usersService;
        await this.drawingSessions[drawingId].load();
        return this.drawingSessions[drawingId];
    }

    public async unloadDrawingSession(drawingId: string): Promise<DrawingSession> {
        // Do nothing if drawing session is not loaded
        if (!this.drawingSessions[drawingId]) { return; }
        // Unload drawing session
        await this.drawingSessions[drawingId].unload();
        delete this.drawingSessions[drawingId];
        // Delete all active user linked to the drawing session
        for (const userId of Object.keys(this.activeCollaborators)) {
            if (this.activeCollaborators[userId] === drawingId) {
                delete this.activeCollaborators[userId];
            }
        }
    }

    private getUserDrawingSession(socket: AuthenticatedSocket): DrawingSession {
        if (!this.activeCollaborators[socket.user.id]) { return null; }
        if (!this.drawingSessions[this.activeCollaborators[socket.user.id]]) { return null; }
        return this.drawingSessions[this.activeCollaborators[socket.user.id]];
    }



    //
    // Handle edits
    //

    private receiveMessage(socket: AuthenticatedSocket, message: EditMessage, callback: (response: SocketMessage) => void) {
        const drawingSession = this.getUserDrawingSession(socket);
        if (!drawingSession) {
            logger.error(`User "${socket.user.username} [${socket.user.id}]" is not associated to any drawing session.`);
            return;
        }

        switch (message.editType) {
            case 'add':
                drawingSession.addLayer(socket, message, callback);
                break;
            case 'modify':
                drawingSession.modifyLayer(socket, message);
                break;
            case 'delete':
                drawingSession.deleteLayer(socket, message);
                break;
            default:
                this.socketsService.logError(`Unknown edit type: "${message.editType}"`, socket);
                return;
        }
    }

}
