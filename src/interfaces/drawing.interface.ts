import { ObjectId } from 'mongoose';
import { Album } from './album.interface';
import { PublicProfile } from './public-profile.interface';

export interface DrawingStore {
    _id: ObjectId;
    id: string;
    album: ObjectId;
    title: string;
    author: ObjectId;
    dateCreated: Date;
    dateModified: Date;
    password: string;
    isExposed: boolean;
}

export interface Drawing {
    id: string;
    album: Album;
    title: string;
    author: PublicProfile;
    activeCollaborators: PublicProfile[];
    dateCreated: number;
    dateModified: number;
    hasPassword: boolean;
    hasAccess: boolean;
    thumbnail: string;
    original: string;
    isExposed: boolean;
}
