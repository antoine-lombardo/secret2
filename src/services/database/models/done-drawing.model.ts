import { DoneDrawingStore } from '@src/interfaces';
import mongoose from 'mongoose';

export const DoneDrawingModel = mongoose.model<DoneDrawingStore>(
    'DoneDrawing',
    new mongoose.Schema({
        title:         String,
        author:        { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        collaborators: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
        dateCreated:   Number,
        likes: [{
            user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
            hidden:    Boolean,
            timestamp: Number
        }],
        comments: [{
            author:    { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
            comment:   String,
            hidden:    Boolean,
            timestamp: Number
        }]
    })
);
