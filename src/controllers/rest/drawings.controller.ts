import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpDelete, httpGet, httpPatch, httpPost, request, requestParam } from 'inversify-express-utils';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { AlbumsService, ChatService, CollabService, DrawingsService, SocketsService, ChatRoomsService, DoneDrawingsService, DrawingImagesService, UsersService } from '@src/services';
import * as HttpErrors from '@src/utils/http-errors';
import { DrawingNotFoundError } from '@src/utils/http-errors';



@controller('/drawings')
export class DrawingsController extends BaseHttpController {

  @inject(DrawingsService) public drawingsService: DrawingsService;
  @inject(DoneDrawingsService) public doneDrawingsService: DoneDrawingsService;
  @inject(DrawingImagesService) public drawingsImageService: DrawingImagesService;
  @inject(AlbumsService) public albumsService: AlbumsService;
  @inject(CollabService) public collabService: CollabService;
  @inject(SocketsService) public socketsService: SocketsService;
  @inject(ChatRoomsService) public chatRoomsService: ChatRoomsService;
  @inject(ChatService) public chatService: ChatService;
  @inject(UsersService) public usersService: UsersService;

  @httpPost('/', AuthMiddleware)
  async create(@request() req: Request) {
    const title: string = req.body.title;
    const albumId: string = req.body.albumId;
    const password: string = req.body.password;

    // Missing parameters
    if (!title) { throw new HttpErrors.MissingDrawingTitleError(); }
    if (!albumId) { throw new HttpErrors.MissingDrawingAlbumIdError(); }

    // Try to find album
    const album = await this.albumsService.findById(albumId, this.httpContext.user.details.id, req);
    if (!album) { throw new HttpErrors.AlbumNotFoundError(); }

    // Check if user has access to the album
    if (!album.hasAccess) { throw new HttpErrors.AlbumMemberPermissionError(); }

    // Create the drawing
    const drawing = await this.drawingsService.create(this.httpContext.user.details.id, album.id, title, password, req);
    if (!drawing) { throw new HttpErrors.DatabaseError(); }

    // Create the chat room
    const chatRoom = await this.chatRoomsService.createDrawingChat(drawing.id);
    if (!chatRoom) {
      this.drawingsService.delete(this.httpContext.user.details.id, drawing.id);
      throw new HttpErrors.DatabaseError();
    }

    return {
      type: 'success',
      description: 'Dessin créé.',
      drawing
    };

  }


  @httpGet('/:id', AuthMiddleware)
  async get(@requestParam('id') id: string, @request() req: Request) {
    const drawingStore = await this.drawingsService.findByID(id);
    if (!drawingStore) { throw new DrawingNotFoundError(); }
    const drawing = await this.drawingsService.storeToDict(drawingStore as any, this.httpContext.user.details.id, req);

    return {
      type: 'success',
      drawing
    };

  }


  @httpPatch('/:id', AuthMiddleware)
  async modify(@requestParam('id') id: string, @request() req: Request) {
    const title: string = req.body.title;
    const albumId: string = req.body.albumId;
    const password: string = (req.body.password) ? req.body.password : null;
    const passwordModified: boolean = req.body.passwordModified;

    // Missing parameters
    if (!title) { throw new HttpErrors.MissingDrawingTitleError(); }
    if (!albumId) { throw new HttpErrors.MissingDrawingAlbumIdError(); }
    if (passwordModified !== true && passwordModified !== false) { throw new HttpErrors.MissingPasswordChangedIndicatorError(); }

    // Try to find album
    const album = await this.albumsService.findById(albumId, this.httpContext.user.details.id, req);
    if (!album) { throw new HttpErrors.AlbumNotFoundError(); }

    // Check if user has access to the album
    if (!album.hasAccess) { throw new HttpErrors.AlbumMemberPermissionError(); }

    // Modify the drawing
    const drawing = await this.drawingsService.modify(id, this.httpContext.user.details.id, album.id, title, password, passwordModified, req);
    if (!drawing) { throw new HttpErrors.DatabaseError(); }

    return {
      type: 'success',
      description: 'Dessin modifié.',
      drawing
    };

  }


  @httpDelete('/:id', AuthMiddleware)
  async delete(@requestParam('id') id: string) {
    // Unload drawing from memory
    await this.collabService.handleDrawingDeleted(id, 'drawing_deleted', 'Ce dessin vient d\'être supprimé.');
    // Unload chat room from memory
    const chatRoom = await this.chatRoomsService.findByDrawingId(id);
    if (chatRoom) { await this.chatService.handleChatDeleted(chatRoom.id); }
    // Delete drawing from database
    if (!(await this.drawingsService.delete(this.httpContext.user.details.id, id))) { throw new HttpErrors.DatabaseError(); }
    // Delete chat room
    await this.chatRoomsService.deleteDrawingRoom(id);
    return {
      type: 'success',
      description: 'Dessin supprimé.'
    };
  }


  @httpGet('/:id/expose', AuthMiddleware)
  async expose(@requestParam('id') id: string, @request() req: Request) {
    const drawing = await this.drawingsService.setExposition(id, this.httpContext.user.details.id, true, req);
    return {
      type: 'success',
      description: 'Dessin ajouté à l\'exposition.',
      drawing
    };
  }


  @httpGet('/:id/unexpose', AuthMiddleware)
  async unexpose(@requestParam('id') id: string, @request() req: Request) {
    const drawing = await this.drawingsService.setExposition(id, this.httpContext.user.details.id, false, req);
    return {
      type: 'success',
      description: 'Dessin retiré de l\'exposition.',
      drawing
    };
  }


  @httpPost('/:id/markAsDone', AuthMiddleware)
  async markAsDone(@requestParam('id') id: string, @request() req: Request) {
    const image: string = req.body.image;

    // Missing parameters
    if (!image) { throw new HttpErrors.MissingImageError(); }

    // Try to find album
    const doneDrawing = await this.doneDrawingsService.create(this.httpContext.user.details.id, id, image, req);
    if (!doneDrawing) { throw new HttpErrors.InternalError(); }

    await this.drawingsService.delete(this.httpContext.user.details.id, id);

    return {
      type: 'success',
      description: 'Dessin terminé.',
      doneDrawing
    };

  }


  @httpPost('/:id/join', AuthMiddleware)
  async post(@requestParam('id') id: string, @request() req: Request) {
    const password: string = req.body.password;
    // Retreive drawing from the database
    const drawingStore = await this.drawingsService.findByID(id);
    if (!drawingStore) { throw new HttpErrors.DrawingNotFoundError(); }
    // Retreive chat room from database
    const chatRoom = await this.chatRoomsService.findByDrawingId(id);
    if (!chatRoom) { throw new HttpErrors.ChatRoomNotFoundError(); }
    // Parse drawing details
    const drawing = await this.drawingsService.storeToDict(drawingStore as any, this.httpContext.user.details.id, req);
    // Check if user has permission to edit the drawing
    if (!drawing.hasAccess) { throw new HttpErrors.AlbumMemberPermissionError(); }
    // Check if user entered the right password
    if (drawing.author.id !== this.httpContext.user.details.id) {
      if (drawing.hasPassword && password !== drawingStore.password) { throw new HttpErrors.InvalidDrawingPasswordError(); }
    }
    // Search for the user socket
    const socket = this.socketsService.getUserSocket(this.httpContext.user.details.id);
    if (!socket) { throw new HttpErrors.SocketDisconnectedError(); }
    // Get active users
    const collaborators = this.collabService.getActiveUsers(drawing.id);
    // Add user to live collab
    await this.collabService.handleJoin(socket, drawing.id);
    // Add user to chat room
    await this.chatService.handleJoin(socket, chatRoom.id);
    // Get layers
    const layers = this.collabService.getLayers(drawing.id);
    // List all active chat users
    const activeUsers = this.chatService.getActiveUsers(chatRoom.id);
    // List messages
    const messages = this.chatService.getMessages(chatRoom.id);
    // Handle join
    await this.usersService.handleEdit(this.httpContext.user.details.id, id);


    return {
      type: 'success',
      description: 'Mode de collaboration activé.',
      layers,
      collaborators,
      chat: {
        chatRoom,
        messages,
        activeUsers
      }
    };
  }


  @httpPost('/quit', AuthMiddleware)
  async quit(@request() req: Request) {
    const image: string = req.body.image;
    // Missing parameters
    if (!image) { throw new HttpErrors.MissingImageError(); }
    // Search for the user socket
    const socket = this.socketsService.getUserSocket(this.httpContext.user.details.id);
    if (!socket) { throw new HttpErrors.SocketDisconnectedError(); }
    // Remove user from live collab
    const drawingId = this.collabService.handleQuit(socket);
    // Save new image
    await this.drawingsImageService.save('active', drawingId, image);
    // Remove user from chat
    await this.chatService.handleQuitDrawing(socket, drawingId);
    return {
      type: 'success',
      description: 'Mode de collaboration désactivé.'
    };
  }

}
