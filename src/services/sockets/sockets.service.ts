import { logger } from '@services/logger';
import { AuthenticatedSocket, SocketMessage, User } from '@src/interfaces';
import { injectable } from 'inversify';
import { Server } from 'socket.io';
import { HttpError } from '@src/utils/http-errors/error-base';
import { formatIpPrefix } from '@src/utils/socket-utils';

@injectable()
export class SocketsService {

    public socketApp: Server;
    private messageHandlers: {[key: string]: (socket: AuthenticatedSocket, message: any, callback: (response: SocketMessage) => void) => void} = {};
    private onConnectHandlers:    ((socket: AuthenticatedSocket) => void)[] = [];
    private onDisconnectHandlers: ((socket: AuthenticatedSocket) => void)[] = [];

    constructor() {}


    // --------------------------------------------------- //
    //                      REGISTERS                      //
    // --------------------------------------------------- //



    public registerApp(app: Server) {
        this.socketApp = app;
    }



    public registerHandler(type: string, handler: (socket: AuthenticatedSocket, message: any, callback: (response: SocketMessage) => void) => void) {
        this.messageHandlers[type] = handler;
    }



    public registerOnConnect(handler: (socket: AuthenticatedSocket) => void) {
        this.onConnectHandlers.push(handler);
    }



    public registerOnDisconnect(handler: (socket: AuthenticatedSocket) => void) {
        this.onDisconnectHandlers.push(handler);
    }





    // --------------------------------------------------- //
    //                      HANDLERS                       //
    // --------------------------------------------------- //



    public socketHandler(socket: AuthenticatedSocket)  {
        if (!socket.authFailed) {
            // ON CONNECT
            for (const handler of this.onConnectHandlers) {
                try {
                    handler(socket);
                } catch {}
            }

            // ON MESSAGE
            socket.on('PUBLIC_MESSAGE', (message: SocketMessage | string, callback: (response: SocketMessage) => void) => {
                let parsedMessage: SocketMessage;
                if ((message as SocketMessage).type) {
                    parsedMessage = message as SocketMessage;
                } else {
                    try {
                        parsedMessage = JSON.parse(message as string);
                        if (!parsedMessage.type) { throw new Error(); }
                    } catch {
                        logger.error(`[SOCKET] Unable to parse message.`);
                        return;
                    }
                }
                if (this.messageHandlers[parsedMessage.type]) {
                    try {
                        this.messageHandlers[parsedMessage.type](socket, parsedMessage, callback);
                    } catch {
                        logger.error(`[SOCKET] Error thrown while processing the message..`);
                    }
                } else {
                    logger.debug(`[SOCKET] Unknown message type: "${parsedMessage.type}"`);
                }
            });

            // ON DISCONNECT
            socket.on('disconnect', () => {
                if (socket.user) {
                    for (const handler of this.onDisconnectHandlers) {
                        try {
                            handler(socket);
                        } catch {}
                    }
                }
                logger.debug(`[SOCKET] ${formatIpPrefix(socket)}${socket.user.username}: Disconnected`);
            });
        }
    }





    // --------------------------------------------------- //
    //                      UTILITIES                      //
    // --------------------------------------------------- //



    public logDebug(message: string, socket: AuthenticatedSocket) {
        logger.debug(`[SOCKET] ${formatIpPrefix(socket)}${socket.user.username}: ${message}`);
    }


    public logError(message: string, socket: AuthenticatedSocket) {
        logger.error(`[SOCKET] ${formatIpPrefix(socket)}${socket.user.username}: ${message}`);
    }


    public async userConnected(user: User): Promise<boolean> {
        let connected = false;
        await this.socketApp.sockets.sockets.forEach((sock: AuthenticatedSocket, sockId) => {
            if (sock.user.id === user.id) { connected = true; }
        });
        return connected;
    }



    public async disconnectUser(user: User): Promise<boolean> {
        let disconnected = false;
        await this.socketApp.sockets.sockets.forEach((sock: AuthenticatedSocket, sockId) => {
            sock.disconnect();
            if (sock.user.id === user.id) {
                sock.disconnect();
                disconnected = true;
            }
        });
        return disconnected;
    }



    public emit(messageObject: any) {
        this.socketApp.emit('PUBLIC_MESSAGE', messageObject);
    }


    public async emitToUser(userId: string, messageObject: any): Promise<boolean> {
        await this.socketApp.sockets.sockets.forEach((sock: AuthenticatedSocket, sockId) => {
            if (sock.user && sock.user.id === userId) {
                sock.emit('PUBLIC_MESSAGE', messageObject);
                return true;
            }
        });
        return false;
    }


    public async emitToAllExcept(messageObject: SocketMessage, exceptUser: User) {
        await this.socketApp.sockets.sockets.forEach((sock: AuthenticatedSocket, sockId) => {
            if (sock.user && sock.user.id !== exceptUser.id) {
                sock.emit('PUBLIC_MESSAGE', messageObject);
            }
        });
    }


    public async emitToAll(messageObject: SocketMessage) {
        await this.socketApp.sockets.sockets.forEach((sock: AuthenticatedSocket, sockId) => {
            sock.emit('PUBLIC_MESSAGE', messageObject);
        });
    }



    public async sendError(error: HttpError, next: (err: Error) => void) {
        const err = new Error(error.errorCode);
        (err as any).data = JSON.stringify({
            name: 'error',
            code: error.errorCode,
            description: error.errorDescription
        });
        logger.debug(`[SOCKET] Error: ${error.errorCode}`);
        next(err);
    }


    public getUserSocket(userId: string): AuthenticatedSocket {
        for (const entry of Array.from(this.socketApp.sockets.sockets)) {
            const socket = entry[1] as AuthenticatedSocket;
            if (socket?.user?.id === userId) { return socket; }
        }
        return null;
    }
}

