import { logger } from '@services/logger';
import { AuthenticatedSocket, ChatMessage } from '@src/interfaces';
import { inject, injectable, LazyServiceIdentifer } from 'inversify';
import { SocketsService } from '@src/services';
import { ChatSession } from '@src/classes/chat-session';
import { ChatMessagesService } from '../database/handlers/chat-messages.service';
import { ChatRoomsService } from '../database/handlers/chat-rooms.service';
import { ChatRoomNotFoundError } from '@src/utils/http-errors/chat-errors';

@injectable()
export class ChatService {
    private ChatSessionConstructor: { new(chatRoomId: string): ChatSession };
    private socketsService: SocketsService;
    private messagesService: ChatMessagesService;
    private roomsService: ChatRoomsService;
    private chatSessions: {[chatRoomId: string]: ChatSession} = {};

    constructor(
        @inject(new LazyServiceIdentifer(() => SocketsService)) socketsService: SocketsService,
        @inject(new LazyServiceIdentifer(() => ChatMessagesService)) messagesService: ChatMessagesService,
        @inject(new LazyServiceIdentifer(() => ChatRoomsService)) roomsService: ChatRoomsService,
        @inject(new LazyServiceIdentifer(() => 'ChatSessionConstructor')) ChatSessionConstructor: { new(): ChatSession }
    ) {
        this.socketsService = socketsService;
        this.messagesService = messagesService;
        this.roomsService = roomsService;
        this.ChatSessionConstructor = ChatSessionConstructor;

        // Register socket handler
        this.socketsService.registerHandler('chat',
            (socket: AuthenticatedSocket, message: ChatMessage) => {
                this.receiveMessage(socket, message);
            }
        );

        // TEMP: Register on disconnect
        this.socketsService.registerOnDisconnect((socket: AuthenticatedSocket) => {
            this.handleDisconnect(socket);
        });

        // TEMP: Register on connect (auto drawing session)
        this.socketsService.registerOnConnect((socket: AuthenticatedSocket) => {
            this.handleJoin(socket, 'public');
        });

    }



    //
    // Handle clients
    //

    public async handleJoin(socket: AuthenticatedSocket, chatRoomId: string) {
        if (chatRoomId === 'public') {
            chatRoomId = await this.roomsService.getPublicChatId();
        }
        if (!chatRoomId) {
            throw new ChatRoomNotFoundError();
        }
        const chatSession = await this.loadChatSession(chatRoomId);
        chatSession.handleJoin(socket);
    }

    public async handleQuit(socket: AuthenticatedSocket, chatRoomId: string) {
        const chatSession = this.chatSessions[chatRoomId];
        if (!chatSession) { return; }
        const chatRoom = await this.roomsService.findByID(chatRoomId);
        chatSession?.handleQuit(socket);
        if (( chatSession && Object.values(chatSession.sockets).length > 0 ) || chatRoom?.isPublicRoom) { return; }
        this.unloadChatSession(chatRoomId);
    }

    public async handleQuitDrawing(socket: AuthenticatedSocket, drawingId: string) {
        const chatRoom = await this.roomsService.findByDrawingId(drawingId);
        if (!chatRoom) { return; }
        await this.handleQuit(socket, chatRoom.id);
    }

    public async handleDisconnect(socket: AuthenticatedSocket) {
        if (!socket.user) { return; }
        for (const chatRoomId of Object.keys(this.chatSessions)) {
            try {
                await this.handleQuit(socket, chatRoomId);
            } catch {}
        }
    }

    public getActiveUsers(chatRoomId: string): string[] {
        if (!this.chatSessions[chatRoomId]) { return []; }
        return Object.keys(this.chatSessions[chatRoomId].sockets);
    }

    public getMessages(chatRoomId: string): ChatMessage[] {
        if (!this.chatSessions[chatRoomId]) { return []; }
        return Object.values(this.chatSessions[chatRoomId].messages);
    }



    //
    // Handle drawing sessions
    //

    public async loadPublicChatSession() {
        const publicChatRoom = await this.roomsService.createPublicChat();
        await this.loadChatSession(publicChatRoom.id);
    }

    public async loadChatSession(chatRoomId: string): Promise<ChatSession> {
        // Do nothing if drawing session is already loaded
        if (this.chatSessions[chatRoomId]) { return this.chatSessions[chatRoomId]; }
        // Load drawing session
        this.chatSessions[chatRoomId] = new this.ChatSessionConstructor(chatRoomId);
        this.chatSessions[chatRoomId].messagesService = this.messagesService;
        this.chatSessions[chatRoomId].socketsService = this.socketsService;
        await this.chatSessions[chatRoomId].load();
        return this.chatSessions[chatRoomId];
    }

    public async unloadChatSession(chatRoomId: string): Promise<void> {
        // Do nothing if drawing session is not loaded
        if (!this.chatSessions[chatRoomId]) { return; }
        // Unload drawing session
        await this.chatSessions[chatRoomId].unload();
        delete this.chatSessions[chatRoomId];
    }


    // Special events

    public async handleChatDeleted(chatRoomId: string) {
        const chatSession = this.chatSessions[chatRoomId];
        if (chatSession) {
            chatSession.handleChatDeleted();
            await this.unloadChatSession(chatRoomId);
        }

        logger.error(`Forcefully unloaded chat session "${chatRoomId}" from memory.`);
    }



    //
    // Handle edits
    //

    private receiveMessage(socket: AuthenticatedSocket, message: ChatMessage) {
        const editedMessage: ChatMessage = {
            userId: socket.user.id,
            roomId: message.roomId,
            message: message.message
        };
        const chatSession = this.chatSessions[editedMessage.roomId];
        if (!chatSession) {
            logger.error(`Chat room "${editedMessage.roomId}" is not loaded in memory.`);
            return;
        }
        if (!chatSession.isConnected(socket)) {
            logger.error(`User "${socket.user.id}" is not connected to chat session "${editedMessage.roomId}".`);
            return;
        }
        chatSession.addMessage(socket, editedMessage);
    }

}
