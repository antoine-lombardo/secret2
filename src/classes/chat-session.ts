import { AuthenticatedSocket, ChatMessage, DrawEventMessage} from '@src/interfaces';
import { SocketsService } from '@src/services';
import { logger } from '@services/logger';
import { ChatEventMessage } from '@src/interfaces/chat-event-message.interface';
import { ChatMessagesService } from '@src/services/database/handlers/chat-messages.service';


export class ChatSession {

    public socketsService: SocketsService;
    public messagesService: ChatMessagesService;

    sockets: {[index: string]: AuthenticatedSocket} = {};
    messages: {[index: string]: ChatMessage} = {};
    chatRoomId: string;

    constructor(chatRoomId: string) {
        this.chatRoomId = chatRoomId;
        this.messages = {};
        this.sockets = {};
    }

    public async load() {
        const messages = await this.messagesService.findByRoomId(this.chatRoomId);
        for (const message of messages) { this.messages[message.id] = message; }
        logger.debug(`Loaded chat room "${this.chatRoomId}" into memory.`);
    }

    public async unload() {
        logger.debug(`Unloaded chat room "${this.chatRoomId}" from memory.`);
    }


    //
    // Active sockets
    //

    handleJoin(socket: AuthenticatedSocket) {
        // If already in, skip
        if (this.sockets[socket.user.id]) { return; }
        // Add to collaborators
        this.sockets[socket.user.id] = socket;
        logger.debug(`User "${socket.user.username}" joined chat room "${this.chatRoomId}"`);
    }

    handleQuit(socket: AuthenticatedSocket) {
        // If already out, skip
        if (!this.sockets[socket.user.id]) { return; }
        // Remove from collaborators
        delete this.sockets[socket.user.id];
        logger.debug(`User "${socket.user.username}" quit chat room "${this.chatRoomId}"`);
    }

    isConnected(socket: AuthenticatedSocket) {
        if (this.sockets[socket.user.id]) {
            return true;
        } else {
            return false;
        }
    }


    //
    // Handling
    //

    async addMessage(socket: AuthenticatedSocket, receivedMessage: ChatMessage) {
        try {
            // Add message to database
            const message = await this.messagesService.addMessage(socket.user.id, this.chatRoomId, receivedMessage.message);
            this.messages[message.id] = message;
            // Emit message
            for (const activeSocket of Object.values(this.sockets)) {
                const emitableMessage = message as any;
                emitableMessage['type'] = 'chat';
                activeSocket.emit('PUBLIC_MESSAGE', emitableMessage);
            }
        } catch {
            this.socketsService.logError('Failed to send the message.', socket);
        }
    }

    //
    // Special event
    //

    async handleChatDeleted() {
         const deletedEvent = {
            type: 'chatEvent',
            chatRoomId: this.chatRoomId,
            eventType: 'kick',
            reason: 'chat_deleted',
            message: 'Une de vos salles de clavardage a été supprimée.'
        };
        for (const activeSocket of Object.values(this.sockets)) {
            activeSocket.emit('PUBLIC_MESSAGE', deletedEvent);
        }
    }

}
