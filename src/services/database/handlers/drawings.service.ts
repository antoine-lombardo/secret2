import { PublicProfile, User } from '@src/interfaces';
import { AlbumMemberPermissionError, AlbumNotFoundError, DatabaseError, DrawingCreatorPermissionError, DrawingNotFoundError } from '@src/utils/http-errors';
import { Request } from 'express';
import { inject, injectable } from 'inversify';
import mongoose from 'mongoose';
import { DrawingModel } from '@src/services/database/models';
import { UsersService } from './users.service';
import { AlbumsService } from './albums.service';
import { Drawing, DrawingStore } from '@src/interfaces/drawing.interface';
import { DrawingImagesService } from '@src/services/drawing-images';

import getDecorators from 'inversify-inject-decorators';
import { container } from '@src/inversify.config';
import { LayersService } from './layers.service';
const {lazyInject} = getDecorators(container);

@injectable()
export class DrawingsService {
    @lazyInject(UsersService) usersService: UsersService;
    @lazyInject(AlbumsService) albumsService: AlbumsService;
    @lazyInject(LayersService) layersService: LayersService;
    @lazyInject(DrawingImagesService) drawingImagesService: DrawingImagesService;


    model = DrawingModel;
    private activeDrawings: {[drawingId: string]: string[]} = {};



    constructor() {
        this.usersService.getUsersDrawingsNumber = async (userId: string) => {
            const drawings = await this.model.find({author: userId});
            return (drawings) ? drawings.length : 0;
        };
    }



    /**
     * Creates a drawing
     * @param userID The user ID of the user creating the album.
     * @return A promise to be the created album.
     */
    async create(userId: string, albumId: string, title: string, password: string, request: Request): Promise<Drawing> {
        if (!password) { password = null; }

        const drawing = new DrawingModel({
            album: albumId,
            title,
            author: userId,
            dateCreated: Date.now(),
            dateModified: Date.now(),
            password,
            isExposed: false
        });
        try {
            return await this.storeToDict(await drawing.save(), userId, request);
        } catch {
            throw new DatabaseError();
        }
    }

    /**
     * Modifies a drawing
     * @return A promise to be the created album.
     */
    async modify(drawingId: string, userId: string, albumId: string, title: string, password: string, passwordModified: boolean, request: Request): Promise<Drawing> {

        const filter: any = {
            album: albumId,
            title,
            dateModified: Date.now(),
        };
        if (passwordModified) { filter['password'] = password; }

        const drawingStore = await this.findByID(drawingId);
        if (!drawingStore) { throw new DrawingNotFoundError(); }
        const modifiedDrawing = await this.model.findByIdAndUpdate(drawingId, filter, {new: true});
        try {
            return await this.storeToDict(modifiedDrawing, userId, request);
        } catch {
            throw new DatabaseError();
        }

    }


    async setExposition(drawingId: string, userId: string, isExposed: boolean, request: Request): Promise<Drawing> {

        const drawingStore = await this.findByID(drawingId);
        if (!drawingStore) { throw new DrawingNotFoundError(); }
        const drawing = await this.storeToDict(drawingStore as any, userId, request);
        if (!drawing.hasAccess) { throw new AlbumMemberPermissionError(); }
        const modifiedDrawing = await this.model.findByIdAndUpdate(drawingId, {isExposed}, {new: true});
        try {
            return await this.storeToDict(modifiedDrawing, userId, request);
        } catch {
            throw new DatabaseError();
        }

    }

    /**
     * Deletes a drawing it the requesting user is the creator.
     * @param drawingID The drawing ID to delete.
     * @param userID The requesting user ID.
     * @return A promise to be true.
     */
    async delete(userId: string, drawingId: string): Promise<boolean> {
        let drawing = null;
        try {
            drawing = await this.model.findById(drawingId);
        } catch {
            throw new DrawingNotFoundError();
        }
        if (!drawing) { throw new DrawingNotFoundError(); }
        if (userId !== drawing.author.toString()) { throw new DrawingCreatorPermissionError(); }
        try {
            await this.layersService.deleteDrawingLayers(drawing.id.toString());
            await drawing.delete();
            return true;
        } catch {
            throw new DatabaseError();
        }
    }

    async fixDrawings(albumId: string, oldAuthor: string, newAuthor: string) {

        await this.model.updateMany(
        {
            album: albumId,
            author: oldAuthor
        },
        {
            author: newAuthor
        });

    }


    /**
     * Deletes all drawings associated to album ID
     * @param albumId The album ID
     * @return A promise to be true.
     */
    async deleteFromAlbum(albumId: string) {
        try {
            const drawings = await this.model.find({album: albumId});
            for (const drawing of drawings) {
                this.layersService.deleteDrawingLayers(drawing.id.toString());
            }
            await this.model.deleteMany({album: albumId});
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns the drawing
     * @param id The drawing ID
     * @return A promise to be the drawing
     */
    async findByID(id: string): Promise<DrawingStore> {
        try {
            const drawingStore = await this.model.findById(id);
            if (!drawingStore) { return null; }
            return drawingStore;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns drawings in provided album
     * @param inAlbumId The album ID
     * @param reqUserId The user ID of the user making the request.
     * @param request The HTTP request.
     * @return A promise to be a list of albums.
     */
    async findInAlbum(inAlbumId: string, reqUserId: string, request: Request): Promise<Drawing[]> {
        try {
            const drawingStores = await this.model.find({album: inAlbumId});
            const drawings = [] as Drawing[];
            for (const drawingStore of drawingStores) {
                drawings.push(await this.storeToDict(drawingStore, reqUserId, request));
            }
            return drawings;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns exposed drawings in provided album
     * @param inAlbumId The album ID
     * @param reqUserId The user ID of the user making the request.
     * @param request The HTTP request.
     * @return A promise to be a list of albums.
     */
    async findExposed(inAlbumId: string, reqUserId: string, request: Request): Promise<Drawing[]> {
        try {
            const drawingStores = await this.model.find({album: inAlbumId, isExposed: true});
            const drawings = [] as Drawing[];
            for (const drawingStore of drawingStores) {
                drawings.push(await this.storeToDict(drawingStore, reqUserId, request));
            }
            return drawings;
        } catch {
            throw new DatabaseError();
        }
    }




    async storeToDict(drawingStore: DrawingStore & mongoose.Document, userId: string, request: Request): Promise<Drawing> {
        // Populate
        const populatedDrawingStore = await drawingStore.populate(['album', 'author']);
        // Get album infos
        const album = await this.albumsService.storeToDict(populatedDrawingStore.album as any, userId, request);
        // Get public profiles
        const author = this.usersService.getPublicProfileFromUser((populatedDrawingStore.author as any)._doc as User, request);
        // Get thumbnail
        const images = this.drawingImagesService.getUrls('active', drawingStore.id, request);

        // Create album structure
        return {
            id: drawingStore.id,
            album,
            title: drawingStore.title,
            author,
            activeCollaborators: await this.getCollaborators(drawingStore.id, request),
            dateCreated: drawingStore.dateCreated.getTime(),
            dateModified: drawingStore.dateModified.getTime(),
            hasPassword: drawingStore.password ? true : false,
            hasAccess: album.hasAccess,
            thumbnail: (images) ? images.thumbnail : null,
            original: (images) ? images.original : null,
            isExposed: (drawingStore.isExposed) ? true : false
        };
    }



    /**
     * Adds an active collaborator
     * @param userId The collaborator ID
     * @param drawingId The drawing ID
     */
    addCollaborator(userId: string, drawingId: string) {
        this.removeCollaborator(userId);
        if (!this.activeDrawings[drawingId]) { this.activeDrawings[drawingId] = [userId]; }
        else { this.activeDrawings[drawingId].push(userId); }
    }



    /**
     * Removes an active collaborator
     * @param userId The collaborator ID
     */
    removeCollaborator(userId: string) {
        for (const drawingId of Object.keys(this.activeDrawings)) {
            const index = this.activeDrawings[drawingId].indexOf(userId, 0);
            if (index > -1) { this.activeDrawings[drawingId].splice(index, 1); }
        }
    }



    /**
     * Returns drawing's active collaborators public profiles
     * @param drawingId The drawing ID
     */
    async getCollaborators(drawingId: string, request: Request): Promise<PublicProfile[]> {
        if (!this.activeDrawings[drawingId]) { return []; }
        const activeCollaborators: PublicProfile[] = [];
        for (const userId of this.activeDrawings[drawingId]) {
            const user = await this.usersService.getPublicProfile(userId, request);
            if (user) { activeCollaborators.push(user); }
        }
        return activeCollaborators;
    }
}

