import bcrypt from 'bcrypt';

import { UserModel } from '@src/services/database/models';

import { AuthenticatedSocket, PrivateProfile, PublicProfile, User } from '@src/interfaces';
import { inject, injectable, LazyServiceIdentifer } from 'inversify';
import { AuthenticationService, logger, SocketsService } from '@src/services';
import { Request } from 'express';
import { config } from '@src/config';
import { formatHostname, formatIpPrefix } from '@src/utils/rest-utils';
import { AvatarNotFoundError, UserNotFoundError } from '@src/utils/http-errors';
import { Document } from 'mongoose';
import * as fs from 'fs';
import * as path from 'path';
import { AvatarsService } from '@src/services/avatars';


const AVATARS_DIR = path.normalize(config.FilesDir + '/avatars');

@injectable()
export class UsersService {


    getUsersDrawingsNumber:     (userId: string) => Promise<number>;
    getUsersDoneDrawingsNumber: (userId: string) => Promise<number>;
    getUsersAlbumsNumber:       (userId: string) => Promise<number>;



    @inject(AuthenticationService) authenticationService: AuthenticationService;
    @inject(AvatarsService) avatarsService: AvatarsService;
    socketsService: SocketsService;


    constructor(@inject(new LazyServiceIdentifer(() => SocketsService)) socketsService: SocketsService) {
        this.socketsService = socketsService;
        this.socketsService.registerOnDisconnect(async (socket: AuthenticatedSocket) => {
            await this.handleLogout(socket.user.id);
        });
    }



    /**
     * Verifies if a user with the provided username exists.
     * @param username The email.
     * @return A promise to be either true or false.
     */
    async usernameExists(username: string): Promise<boolean> {
        const escapedUsername = this.escapeRegexp(username);
        const result = await UserModel.findOne({username: {$regex : new RegExp(`^${escapedUsername}$`, 'i')}}).select('_id').lean();
        if (result) { return true; }
        else { return false; }
    }



    /**
     * Verifies if a user with the provided email exists.
     * @param email The email.
     * @return A promise to be either true or false.
     */
    async emailExists(email: string): Promise<boolean> {
        const escapedEmail = this.escapeRegexp(email);
        const result = await UserModel.findOne({email: {$regex : new RegExp(`^${escapedEmail}$`, 'i')}}).select('_id').lean();
        if (result) { return true; }
        else { return false; }
    }



    /**
     * Creates a new user entry.
     * @param username The username.
     * @param email The email.
     * @param password The password.
     * @return A promise to be if user has been successfully added.
     */
    async createUser(username: string, email: string, password: string, darkMode: boolean, avatarType: string, avatarValue: string) {
        // Handle preset avatars
        let originalAvatarFilename = 'fake';
        let resizedAvatarFilename = 'fake';
        if (avatarType === 'preset') {
            originalAvatarFilename = `/base/${avatarValue}/original.png`;
            resizedAvatarFilename = `/base/${avatarValue}/resized.png`;
            if (!fs.existsSync(`${AVATARS_DIR}${originalAvatarFilename}`) || !fs.existsSync(`${AVATARS_DIR}${resizedAvatarFilename}`)) {
                throw new AvatarNotFoundError();
            }
        }

        // Save user
        const user = new UserModel({
            username,
            email,
            privateMode: false,
            darkMode,
            password: this.authenticationService.hashPassword(password),
            originalAvatarFilename,
            resizedAvatarFilename,
            statsCollabsNumber: 0,
            statsTotalTime: 0,
            statsMeanTime: 0,
            statsDrawingsNumber: 0,
            statsDrawings: [],
            logins: [],
            logouts: [],
            editions: []
        });
        const savedUser = await user.save();

        // Handle uploaded avatar
        if (avatarType === 'upload') {
            try {
                const avatar = await this.avatarsService.saveAvatar(user.id.toString(), avatarValue);
                await this.setAvatarFilenames(user.id.toString(), avatar.original, avatar.resized);
            } catch (err) {
                await savedUser.delete();
                throw err;
            }
        }
    }


    async updateStats(userId: string, drawingId: string, timeSpent: number) {
        const tmpUser = await this.findUserById(userId);

        // Create filter
        const filter: any = {
            statsDrawingsNumber: tmpUser.statsDrawingsNumber,
            statsCollabsNumber: tmpUser.statsCollabsNumber + 1,
            statsTotalTime: tmpUser.statsTotalTime + timeSpent,
        };

        // Add drawing if non existent
        let found = false;
        for (const drawing of tmpUser.statsDrawings) {
            if (drawing === drawingId) { found = true; }
        }
        if (!found) {
            filter['$push'] = { statsDrawings: drawingId };
            filter['statsDrawingsNumber'] = filter['statsDrawingsNumber'] + 1;
        }

        // Update mean time
        filter['statsMeanTime'] = filter['statsTotalTime'] / filter['statsCollabsNumber'];

        await tmpUser.updateOne(filter, {new: true});
        logger.debug(`STATS: ${tmpUser.username} stats updated.`);
    }


    async handleLogin(userId: string) {

        const user = await UserModel.findByIdAndUpdate(userId, { $push: { logins: Date.now() } });
        logger.debug(`HISTORY: ${user.username} login handled.`);

    }

    async handleLogout(userId: string) {

        const user = await UserModel.findByIdAndUpdate(userId, { $push: { logouts: Date.now() } });
        logger.debug(`HISTORY: ${user.username} logout handled.`);

    }

    async handleEdit(userId: string, drawingId: string) {

        const user = await UserModel.findByIdAndUpdate(userId, { $push: { editions: { drawingId, timestamp: Date.now() } } });
        logger.debug(`HISTORY: ${user.username} edition handled.`);

    }



    /**
     * Modifies a user entry
     * @param id The user id
     * @param username The new username.
     * @param email The new email.
     * @param password The new password.
     * @return A promise to be if user has been successfully added.
     */
    async modifyUser(id: string, username: string, email: string, password: string | null, privateMode: boolean, darkMode: boolean | null, req: Request): Promise<PrivateProfile> {
        const tmpUser = await this.findUserById(id);
        if (!tmpUser) { throw new UserNotFoundError(); }

        const filter: any = { username, email, privateMode };
        if (password) { filter['password'] = this.authenticationService.hashPassword(password); }
        if (darkMode !== null) { filter['darkMode'] = darkMode; }

        const user = await UserModel.findByIdAndUpdate(id, filter, { new: true });

        logger.debug(`USER: ${user.username} user modified.`);

        return await this.getPrivateProfileFromUser(user, req);
    }



    /**
     * Finds the user from the provided username.
     * @param username The username of the user.
     * @return A promise to be either the user or null if not found.
     */
    async findUserByUsername(username: string): Promise<User> {
        const escapedUsername = this.escapeRegexp(username);
        return (await UserModel.findOne({username: {$regex : new RegExp(`^${escapedUsername}$`, 'i')}}) as User);
    }


    /**
     * Finds the user from the provided email.
     * @param email The email of the user.
     * @return A promise to be either the user or null if not found.
     */
    async findUserByEmail(email: string): Promise<User> {
        const escapedEmail = this.escapeRegexp(email);
        return (await UserModel.findOne({email: {$regex : new RegExp(`^${escapedEmail}$`, 'i')}}) as User);
    }



    /**
     * Finds the user from the provided id.
     * @param id The id of the user.
     * @return A promise to be either the user or null if not found.
     */
    async findUserById(id: string): Promise<Document<unknown, any, User> & User> {
        const user = await UserModel.findById(id);
        return (await UserModel.findById(id));
    }



    /**
     * Searches users by username and email
     * @param query The query
     * @return A promise to be a list of users
     */
    async searchUser(query: string, request: Request): Promise<PublicProfile[]> {
        const users = await UserModel.find({$or: [{username: {$regex : new RegExp(`${query}`, 'i')}}, {email: {$regex : new RegExp(`${query}`, 'i')}}]}) as User[];
        const publicProfiles = [];
        for (const user of users) {
            publicProfiles.push(this.getPublicProfileFromUser(user, request));
        }
        return publicProfiles;
    }



    /**
     * Sets the avatar filenames
     * @param id The id of the user.
     * @param presetTag The preset avatar tag.
     */
    async setPresetAvatar(id: string, presetTag: string) {
        return await this.setAvatarFilenames(id, `/base/${presetTag}/original.png`, `/base/${presetTag}/resized.png`);
    }



    /**
     * Sets the avatar filenames
     * @param id The id of the user.
     * @param originalAvatarFilename The original avatar filename.
     * @param resizedAvatarFilename The original avatar filename.
     */
    async setAvatarFilenames(id: string, originalAvatarFilename: string, resizedAvatarFilename: string) {
        if (!fs.existsSync(`${AVATARS_DIR}${originalAvatarFilename}`) || !fs.existsSync(`${AVATARS_DIR}${resizedAvatarFilename}`)) {
            throw new AvatarNotFoundError();
        }
        return (await UserModel.findByIdAndUpdate(id, {originalAvatarFilename, resizedAvatarFilename}));
    }



    /**
     * Returns user public profile
     * @param id The user id.
     * @returns The public profile, or null if user not found.
     */
    async getPublicProfile(id: string, request: Request): Promise<PublicProfile> {
        const user = await this.findUserById(id);
        if (!user) { return null; }
        return this.getPublicProfileFromUser(user, request);
    }



    /**
     * Returns user public profile
     * @param user The user.
     * @returns The public profile, or null if user not found.
     */
    public getPublicProfileFromUser(user: User, request: Request): PublicProfile {
        return {
            id: user.id || user._id.toString(),
            username: user.username,
            email: (user.privateMode) ? null : user.email,
            privateMode: user.privateMode,
            avatar: {
                original: formatHostname(request) + '/files/avatars' + user.originalAvatarFilename,
                resized: formatHostname(request) + '/files/avatars' + user.resizedAvatarFilename,
            }
        };
    }




    /**
     * Returns user private profile
     * @param id The user id.
     * @returns The private profile, or null if user not found.
     */
    async getPrivateProfile(id: string, request: Request): Promise<PrivateProfile> {
        const user = await this.findUserById(id);
        if (!user) { return null; }
        return await this.getPrivateProfileFromUser(user, request);
    }



    /**
     * Returns user public profile
     * @param user The user.
     * @returns The public profile, or null if user not found.
     */
    private async getPrivateProfileFromUser(user: User, request: Request): Promise<PrivateProfile> {
        const history: { type: string; timestamp: number; link: string }[] = [];
        for (const login of user.logins) {
            history.push({ type: 'login', timestamp: login, link: '' });
        }
        for (const logout of user.logouts) {
            history.push({ type: 'logout', timestamp: logout, link: '' });
        }
        for (const edition of user.editions) {
            history.push({ type: 'edit', timestamp: edition.timestamp, link: edition.drawingId });
        }
        history.sort((a: any, b: any) => {
            if (a.timestamp < b.timestamp) {
                return -1;
            }
            if (a.timestamp > b.timestamp) {
                return 1;
            }
            return 0;
        });
        history.reverse();
        return {
            id: user.id,
            username: user.username,
            email: user.email,
            privateMode: user.privateMode,
            darkMode: user.darkMode,
            avatar: {
                original: formatHostname(request) + '/files/avatars' + user.originalAvatarFilename,
                resized: formatHostname(request) + '/files/avatars' + user.resizedAvatarFilename,
            },
            stats: {
                drawingsNumber: user.statsDrawingsNumber,
                meanTime: user.statsMeanTime,
                totalTime: user.statsTotalTime,
                collabNumber: user.statsCollabsNumber,
                drawingAuthorNumber: await this.getUsersDrawingsNumber(user.id) + await this.getUsersDoneDrawingsNumber(user.id),
                albumMemberNumber: await this.getUsersAlbumsNumber(user.id)
            },
            history
        };
    }



    /**
     * Escapes all regex characters of the provided string
     * @param regex The regex string.
     * @return The escaped string.
     */
    private escapeRegexp(regex: string): string {
        return regex.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
}

