import { ObjectId } from 'mongoose';

export interface LikeStore {
    id: string;
    user: ObjectId;
    hidden: boolean;
    timestamp: number;
}
