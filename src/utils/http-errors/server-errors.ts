import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Parameters

export class InternalError extends HttpError {
    constructor() {
        super(StatusCodes.INTERNAL_SERVER_ERROR, 'INTERNAL', 'Erreur interne.');
    }
}

export class BadRequestError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'BAD_REQUEST', 'Mauvaise requete.');
    }
}
