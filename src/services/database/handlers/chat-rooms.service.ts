import { DatabaseError, DrawingNotFoundError } from '@src/utils/http-errors';
import { inject, injectable, } from 'inversify';
import mongoose from 'mongoose';
import { UsersService } from './users.service';
import { DrawingsService } from './drawings.service';
import { ChatRoom, ChatRoomStore } from '@src/interfaces/chat-room.interface';
import { ChatRoomModel } from '../models/chat-room.model';
import { CannotDeleteDrawingRoomError, CannotDeletePublicRoomError, ChatRoomAuthorPermissionError, ChatRoomNotFoundError } from '@src/utils/http-errors/chat-errors';
import { ChatMessagesService } from './chat-messages.service';

@injectable()
export class ChatRoomsService {
    @inject(UsersService) usersService: UsersService;
    @inject(DrawingsService) drawingsService: DrawingsService;
    @inject(ChatMessagesService) chatMessagesService: ChatMessagesService;


    model = ChatRoomModel;



    constructor() {}



    /**
     * Creates a private chat
     * @return A promise to be the created chat room.
     */
    async createPrivateChat(creatorId: string, title: string, password: string | null): Promise<ChatRoom> {
        if (!password) { password = null; }

        const chatRoom = new ChatRoomModel({
            creator: creatorId,
            title: title,
            isPublicRoom: false,
            isDrawingRoom: false,
            drawing: null,
            password: password
        });
        try {
            return this.storeToDict(await chatRoom.save());
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Modifies a private chat
     * @return A promise to be the created chat room.
     */
    async modifyPrivateChat(userId: string, roomId: string, title: string, passwordModified: boolean, password: string | null): Promise<ChatRoom> {
        // Create filter
        if (!password) { password = null; }
        const filter: any = { title };
        if (passwordModified) { filter['password'] = password; }

        // Check if it exist
        const tmpChatRoom = await this.findByID(roomId);
        if (!tmpChatRoom) { throw new ChatRoomNotFoundError(); }

        // Check if user has right to edit
        if (tmpChatRoom.creatorId !== userId) { throw new ChatRoomAuthorPermissionError(); }

        // Edit the chat room
        try {
            const chatRoom = await this.model.findByIdAndUpdate(roomId, filter, { new: true });
            return this.storeToDict(chatRoom);
        } catch {
            throw new DatabaseError();
        }

    }



    /**
     * Creates a drawing chat
     * @return A promise to be the created chat room.
     */
    async createDrawingChat(drawingId: string): Promise<ChatRoom> {
        const drawing = await this.drawingsService.findByID(drawingId);
        if (!drawing) { throw new DrawingNotFoundError(); }

        const chatRoom = new ChatRoomModel({
            creator: drawing.author.toString(),
            title: null,
            isPublicRoom: false,
            isDrawingRoom: true,
            drawing: drawing.id,
            password: null
        });
        try {
            return this.storeToDict(await chatRoom.save());
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Creates the public chat room if it does not exist
     * @return A promise to be the public chat room.
     */
    async createPublicChat(): Promise<ChatRoom> {
        const existingChatRoom = await this.findPublicChat();
        if (existingChatRoom) { return existingChatRoom; }

        const chatRoom = new ChatRoomModel({
            creator: null,
            title: 'Salle de clavardage publique',
            isPublicRoom: true,
            isDrawingRoom: false,
            drawing: null,
            password: null
        });
        try {
            return this.storeToDict(await chatRoom.save());
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Returns the chat room
     * @param id The chat room id
     * @return A promise to be the chat room
     */
    async findByID(id: string): Promise<ChatRoom> {
        try {
            const chatRoom = await this.model.findById(id);
            if (!chatRoom) { return null; }
            return await this.storeToDict(chatRoom);
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Returns all privatethe chat room
     * @param id The chat room id
     * @return A promise to be the chat room
     */
    async findAllPrivate(): Promise<ChatRoom[]> {
        try {
            const chatRoomStores = await this.model.find({isDrawingRoom: false, isPublicRoom: false});
            if (!chatRoomStores) { return []; }
            const chatRooms: ChatRoom[] = [];
            for (const chatRoomStore of chatRoomStores) {
                chatRooms.push(await this.storeToDict(chatRoomStore));
            }
            return chatRooms;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns the chat room
     * @param id The chat room id
     * @return A promise to be the chat room
     */
    async findByDrawingId(drawingId: string): Promise<ChatRoom> {
        try {
            const chatRoom = await this.model.findOne({drawing: drawingId});
            if (!chatRoom) { return null; }
            return await this.storeToDict(chatRoom);
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns the public chat
     * @return A promise to be the public chat room.
     */
    async findPublicChat(): Promise<ChatRoom> {
        try {
            const chatRoomStore = await this.model.findOne({isPublicRoom: true});
            if (!chatRoomStore) { return null; }
            return this.storeToDict(chatRoomStore);
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Returns the public chat id
     * @return A promise to be the public chat room id.
     */
    async getPublicChatId(): Promise<string> {
        try {
            const chatRoomStore = await this.model.findOne({isPublicRoom: true});
            if (!chatRoomStore) { return null; }
            return chatRoomStore.id;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Deletes a chat room if the requesting user is the creator.
     * @param drawingID The drawing ID to delete.
     * @param userID The requesting user ID.
     * @return A promise to be true.
     */
    async delete(userId: string, chatRoomId: string): Promise<boolean> {
        let chatRoom = null;
        try {
            chatRoom = await this.model.findById(chatRoomId);
        } catch {
            throw new ChatRoomNotFoundError();
        }
        if (!chatRoom) { throw new ChatRoomNotFoundError(); }
        if (chatRoom.isPublicRoom) { throw new CannotDeletePublicRoomError(); }
        if (chatRoom.isDrawingRoom) { throw new CannotDeleteDrawingRoomError(); }
        if (userId !== chatRoom.creator.toString()) { throw new ChatRoomAuthorPermissionError(); }
        try {
            await chatRoom.delete();
            await this.chatMessagesService.deleteRoom(chatRoomId);
            return true;
        } catch {
            throw new DatabaseError();
        }
    }

    /**
     * Deletes a chat room
     * @param drawingID The drawing ID to delete.
     * @return A promise to be true.
     */
    async deleteDrawingRoom(drawingId: string): Promise<boolean> {
        let chatRoom = null;
        try {
            chatRoom = await this.model.findOne({drawing: drawingId});
        } catch {
            throw new ChatRoomNotFoundError();
        }
        try {
            await chatRoom.delete();
            await this.chatMessagesService.deleteRoom(chatRoom.id.toString());
            return true;
        } catch {
            throw new DatabaseError();
        }
    }



    async checkPassword(id: string, password: string): Promise<boolean> {
        let chatRoom = null;
        try {
            chatRoom = await this.model.findById(id);
        } catch {
            throw new ChatRoomNotFoundError();
        }
        if (!chatRoom) { throw new ChatRoomNotFoundError(); }
        if (!chatRoom.password) { return true; }
        return (chatRoom.password === password);
    }



    storeToDict(chatRoomStore: ChatRoomStore & mongoose.Document): ChatRoom {
        if (!chatRoomStore) { return null; }
        // Create chat room structure
        return {
            id: chatRoomStore.id,
            title: chatRoomStore.title,
            creatorId: (chatRoomStore.creator) ? chatRoomStore.creator.toString() : null,
            isPublicRoom: chatRoomStore.isPublicRoom,
            isDrawingRoom: chatRoomStore.isDrawingRoom,
            drawingId: (chatRoomStore.isDrawingRoom) ? chatRoomStore.drawing.toString() : null,
            hasPassword: (chatRoomStore.password) ? true : false
        };
    }
}

