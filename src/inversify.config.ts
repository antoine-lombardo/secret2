import { Container } from 'inversify';
export const container = new Container();


import {
    AuthenticationService,
    UsersService,
    ChatService,
    DatabaseService,
    DrawingsService,
    ServerService,
    SocketsService,
    AlbumsService,
    LayersService,
    CollabService,
    JoinRequestsService,
    AvatarsService,
    NotificationsService,
    DoneDrawingsService,
    DrawingImagesService}
    from '@src/services';
import { AlbumsController, AuthController, AvatarsController, UsersController, ProfileController, DrawingsController, NotificationsController } from './controllers';
import { AuthWSMiddleware } from './middleware/websocket/auth.ws.middleware';
import { AuthMiddleware } from './middleware/rest/auth.middleware';
import { JoinRequestsController } from './controllers/rest/join-requests.controller';
import { DrawingSession } from './classes/drawing-session';
import { ChatSession } from './classes/chat-session';
import { ChatRoomsService } from './services/database/handlers/chat-rooms.service';
import { ChatMessagesService } from './services/database/handlers/chat-messages.service';
import { ChatController } from './controllers/rest/chat.controller';
import { DoneDrawingsController } from './controllers/rest/done-drawings.controller';


// Server
container.bind<ServerService>(ServerService).toSelf().inSingletonScope();

// Controllers
container.bind<AuthController>(AuthController).toSelf().inSingletonScope();
container.bind<AlbumsController>(AlbumsController).toSelf().inSingletonScope();
container.bind<AvatarsController>(AvatarsController).toSelf().inSingletonScope();
container.bind<DrawingsController>(DrawingsController).toSelf().inSingletonScope();
container.bind<UsersController>(UsersController).toSelf().inSingletonScope();
container.bind<ProfileController>(ProfileController).toSelf().inSingletonScope();
container.bind<JoinRequestsController>(JoinRequestsController).toSelf().inSingletonScope();
container.bind<ChatController>(ChatController).toSelf().inSingletonScope();
container.bind<NotificationsController>(NotificationsController).toSelf().inSingletonScope();
container.bind<DoneDrawingsController>(DoneDrawingsController).toSelf().inSingletonScope();


// Middlewares
container.bind<AuthMiddleware>(AuthMiddleware).toSelf().inSingletonScope();
container.bind<AuthWSMiddleware>(AuthWSMiddleware).toSelf().inSingletonScope();


// Services
container.bind<AuthenticationService>(AuthenticationService).toSelf().inSingletonScope();
container.bind<AvatarsService>(AvatarsService).toSelf().inSingletonScope();
container.bind<DatabaseService>(DatabaseService).toSelf().inSingletonScope();
container.bind<AlbumsService>(AlbumsService).toSelf().inSingletonScope();
container.bind<JoinRequestsService>(JoinRequestsService).toSelf().inSingletonScope();
container.bind<LayersService>(LayersService).toSelf().inSingletonScope();
container.bind<UsersService>(UsersService).toSelf().inSingletonScope();
container.bind<DrawingsService>(DrawingsService).toSelf().inSingletonScope();
container.bind<SocketsService>(SocketsService).toSelf().inSingletonScope();
container.bind<ChatService>(ChatService).toSelf().inSingletonScope();
container.bind<CollabService>(CollabService).toSelf().inSingletonScope();
container.bind<ChatRoomsService>(ChatRoomsService).toSelf().inSingletonScope();
container.bind<ChatMessagesService>(ChatMessagesService).toSelf().inSingletonScope();
container.bind<NotificationsService>(NotificationsService).toSelf().inSingletonScope();
container.bind<DoneDrawingsService>(DoneDrawingsService).toSelf().inSingletonScope();
container.bind<DrawingImagesService>(DrawingImagesService).toSelf().inSingletonScope();

// Constructors
container.bind<{ new(drawingId: string): DrawingSession }>('DrawingSessionConstructor').toConstructor(DrawingSession);
container.bind<{ new(drawingId: string): ChatSession }>('ChatSessionConstructor').toConstructor(ChatSession);


