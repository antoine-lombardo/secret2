import { SocketMessage } from './socket-message.interface';

export interface ChatEventMessage extends SocketMessage {
    eventType: string;
    userId: string;
}
