import { inject } from 'inversify';
import { BaseHttpController, controller, httpDelete, httpGet, requestParam } from 'inversify-express-utils';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { NotificationsService } from '@src/services';
import * as HttpErrors from '@src/utils/http-errors';



@controller('/notifications')
export class NotificationsController extends BaseHttpController {


  @inject(NotificationsService) public notificationsService: NotificationsService;



  @httpGet('/', AuthMiddleware)
  async get() {
    const notifications = await this.notificationsService.getAll(this.httpContext.user.details.id);
    return {
      type: 'success',
      notifications
    };
  }

  @httpDelete('/:id', AuthMiddleware)
  async delete(@requestParam('id') id: string) {
    const notification = await this.notificationsService.findById(id);
    if (!notification) { throw new HttpErrors.NotificationNotFoundError(); }
    this.notificationsService.delete(id);

    return {
      type: 'success',
      description: 'Notification supprimée avec succès.'
    };
  }



}
