import { ObjectId } from 'mongoose';
import { SocketMessage } from './socket-message.interface';

export interface NotificationStore {
    _id: ObjectId;
    id: string;
    userId: ObjectId;
    notificationType: string;
    title: string;
    description: string;
    timestamp: number;
    linkType: string;
    linkData: string;
}

export interface Notification extends SocketMessage {
    id: string;
    notificationType: string;
    title: string;
    description: string;
    timestamp: number;
    linkType: string;
    linkData: string;
}
