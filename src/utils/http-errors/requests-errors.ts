import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Parameters

export class MissingFieldsError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_FIELDS', 'Certains champs sont manquants.');
    }
}
