import { ObjectId } from 'mongoose';
import { Coordinates } from './coordinates.interface';
import { PublicProfile } from './public-profile.interface';
import { SocketMessage } from './socket-message.interface';

export interface EditMessage extends SocketMessage {
    // Base
    editType: string;
    // Modifiable
    editing?: boolean;
    layerType?: string;
    strokeColor?: string;
    fillColor?: string;
    isFilled?: boolean;
    isStroked?: boolean;
    isSelecting?: boolean;
    strokeThickness?: number;
    path?: Coordinates[];
    // Non modifiable
    layerId?: string;
    creator?: string;
    editor?: string;
    timestamp?: number;
}
