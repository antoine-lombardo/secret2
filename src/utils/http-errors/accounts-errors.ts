import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Session

export class AlreadyLoggedInError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'ALREADY_LOGGED_IN', 'Une autre session est déjà ouverte.');
    }
}

// User

export class UserNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'USER_NOT_FOUND', 'Cet utilisateur est introuvable.');
    }
}

export class MissingUserIDError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_USER_ID', 'ID de l\'utilisateur manquant.');
    }
}

// Username

export class UsernameNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'USERNAME_NOT_FOUND', 'Ce nom d\'utilisateur est introuvable.');
    }
}

export class EmailNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'EMAIL_NOT_FOUND', 'Cette adresse courriel est invalide.');
    }
}

export class UsernameAlreadyExistError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'USERNAME_ALREADY_EXIST', 'Ce nom d\'utilisateur existe déjà.');
    }
}

export class MissingUsernameError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_USERNAME', 'Nom d\'utilisateur manquant.');
    }
}

// Password

export class MissingPasswordError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_PASSWORD', 'Mot de passe manquant.');
    }
}

export class InvalidPasswordError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'INVALID_PASSWORD', 'Mot de passe invalide.');
    }
}

// Access token

export class MissingAccessTokenError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_ACCESS_TOKEN', 'Jeton d\'authentification manquant.');
    }
}

export class InvalidAccessTokenError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'INVALID_ACCESS_TOKEN', 'Session expirée.');
    }
}

// Email

export class EmailAlreadyUsedError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'EMAIL_ALREADY_USED', 'Cette adresse courriel est déjà utilisée par un autre utilisateur.');
    }
}

export class MissingEmailError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_EMAIL', 'Adresse courriel manquante.');
    }
}
