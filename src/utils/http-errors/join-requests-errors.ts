import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

export class AnotherRequestPendingError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'ANOTHER_REQUEST_PENDING', 'Une autre requête est en attente.');
    }
}

export class AlreadyHasAccessRequestError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'ALREADY_HAS_ACCESS_REQUEST', 'Vous avez déjà accès à cet album.');
    }
}

export class AlreadyMemberRequestError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'ALREADY_MEMBER_REQUEST', 'L\'utilisateur a déjà accès à cet album.');
    }
}

export class AlreadyAnsweredRequestError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'ALREADY_ANSWERED_REQUEST_ERROR', 'Une réponse a déjà été envoyée pour cette demande.');
    }
}

export class JoinRequestNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'JOIN_REQUEST_NOT_FOUND', 'Impossible de trouver la demander d\'adhésion.');
    }
}
