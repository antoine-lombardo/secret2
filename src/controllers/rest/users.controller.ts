import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpGet, request, requestParam } from 'inversify-express-utils';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { UsersService } from '@src/services';
import * as HttpErrors from '@src/utils/http-errors';



@controller('/users')
export class UsersController extends BaseHttpController {



  @inject(UsersService) public usersService: UsersService;



  @httpGet('/:id', AuthMiddleware)
  async infos(@requestParam('id') id: string, @request() req: Request) {
    const publicProfile = await this.usersService.getPublicProfile(id, req);
    if (!publicProfile) {
      throw new HttpErrors.UserNotFoundError();
    }
    return {
      type: 'success',
      publicProfile
    };
  }



  @httpGet('/search/:query', AuthMiddleware)
  async search(@requestParam('query') query: string, @request() req: Request) {
    return {
      type: 'success',
      users: await this.usersService.searchUser(query, req)
    };
  }



}
