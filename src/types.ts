const TYPES = {
    ChatService: Symbol.for('ChatService'),
    DatabaseService: Symbol.for('DatabaseService'),
    LoggerService: Symbol.for('LoggerService'),
    RestServerService: Symbol.for('RestServerService'),
    SocketServerService: Symbol.for('SocketServerService'),
    LoginController: Symbol.for('LoginController'),
    IndexController: Symbol.for('IndexController'),
};

export { TYPES };
