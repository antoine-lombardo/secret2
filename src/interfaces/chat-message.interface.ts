import { ObjectId } from 'mongoose';

export interface ChatMessageStore {
    _id: ObjectId;
    id: string;
    user: ObjectId;
    room: ObjectId;
    message: string;
    timestamp: number;
}

export interface ChatMessage {
    id?: string;
    userId?: string;
    roomId: string;
    message: string;
    timestamp?: number;
}
