import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Not found

export class ChatRoomNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'CHAT_ROOM_NOT_FOUND', 'Impossible de trouver la salle de clavardage.');
    }
}

export class ChatMessageNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'CHAT_MESSAGE_NOT_FOUND', 'Impossible de trouver le message.');
    }
}

export class CannotDeletePublicRoomError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'CANNOT_DELETE_PUBLIC_ROOM', 'Impossible de supprimer la salle de clavardage publique');
    }
}

export class CannotLeavePublicRoomError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'CANNOT_LEAVE_PUBLIC_ROOM', 'Impossible de quitter la salle de clavardage publique');
    }
}

export class CannotDeleteDrawingRoomError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'CANNOT_DELETE_DRAWING_ROOM', 'Impossible de supprimer une salle de clavardage liée à un dessin.');
    }
}

export class ChatRoomAuthorPermissionError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'CHAT_ROOM_AUTHOR_PERMISSION', 'Vous devez être le créateur de la salle de clavardage pour effectuer cette action.');
    }
}

export class WrongChatRoomPasswordError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'WRONG_CHAT_ROOM_PASSWORD', 'Mot de passe de la salle de clavardage incorrect.');
    }
}

export class MissingChatRoomTitleError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_CHAT_ROOM_TITLE', 'Titre de la salle de clavardage manquant.');
    }
}

export class MissingChatRoomIDError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_CHAT_ROOM_ID', 'ID de la salle de clavardage manquant.');
    }
}

export class MissingChatRoomPasswordModifiedError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_CHAT_ROOM_PASSWORD_MODIFIED', 'Indicateur de mot de passe modifié manquant.');
    }
}

