export interface PublicProfile {
    id: string;
    username: string;
    email: string | null;
    privateMode: boolean;
    avatar: {
        original: string;
        resized: string
    };
}
