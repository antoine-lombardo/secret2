export interface DrawingImage {
    original: string;
    thumbnail: string;
}
