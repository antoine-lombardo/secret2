import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Missing Fields

export class MissingDrawingTitleError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_DRAWING_TITLE', 'Titre du dessin manquant.');
    }
}

export class MissingDrawingDescriptionError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_DRAWING_DESCRIPTION', 'Description du dessin manquante.');
    }
}

export class MissingDrawingAlbumIdError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_DRAWING_ALBUM_ID', 'Album manquant.');
    }
}

// Not found

export class DrawingNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'Drawing_NOT_FOUND', 'Impossible de trouver le dessin.');
    }
}

// Permission

export class DrawingCreatorPermissionError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'DRAWING_CREATOR_PERMISSION', 'Vous devez être le créateur du dessin pour faire cela.');
    }
}

export class InvalidDrawingPasswordError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'INVALID_DRAWING_PASSWORD', 'Mot de passe incorrect.');
    }
}

// Socket

export class SocketDisconnectedError extends HttpError {
    constructor() {
        super(StatusCodes.CONFLICT, 'SOCKET_DISCONNECTED_ERROR', 'Le socket est déconnecté.');
    }
}

// thumbnails

export class InvalidImageDataError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'INVALID_AVATAR_DATA_ERROR', 'Impossible de décoder l\'image.');
    }
}


export class InvalidImageTypeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'INVALID_AVATAR_TYPE_ERROR', 'Type d\'image invalide.');
    }
}


export class MissingImageError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_IMAGE_ERROR', 'Image manquante.');
    }
}


export class MissingDateError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_DATE_ERROR', 'Date manquante.');
    }
}


export class MissingCommentError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_COMMENT_ERROR', 'Commentaire manquant.');
    }
}


export class CommentCreatorPermissionError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'COMMENT_CREATOR_PERMISSION', 'Vous devez être l\'auteur du commentaire pour faire cela.');
    }
}

export class CommentNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'COMMENT_NOT_FOUND', 'Commentaire introuvable.');
    }
}
