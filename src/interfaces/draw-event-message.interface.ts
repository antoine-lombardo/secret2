import { SocketMessage } from './socket-message.interface';

export interface DrawEventMessage extends SocketMessage {
    eventType: string;
    reason?: string;
    message?: string;
    userId?: string;
}
