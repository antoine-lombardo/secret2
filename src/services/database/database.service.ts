import mongoose from 'mongoose';
import { config } from '@src/config';
import { logger } from '@services/logger';
import { injectable } from 'inversify';

@injectable()
export class DatabaseService {

    connect(callback: () => void) {
        const _this = this;
        mongoose.connect(`mongodb+srv://${config.MongoUser}:${config.MongoPassword}${config.MongoPath}`).then(() => {
            logger.info('Connected to MongoDB server.');
            callback();
        });
    }
}
