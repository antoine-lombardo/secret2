import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpDelete, httpGet, httpPost, httpPut, request, requestParam } from 'inversify-express-utils';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { DoneDrawingsService, DrawingImagesService, NotificationsService } from '@src/services';
import { DrawingNotFoundError, MissingCommentError, MissingDateError } from '@src/utils/http-errors';



@controller('/doneDrawings')
export class DoneDrawingsController extends BaseHttpController {

  @inject(DoneDrawingsService) public doneDrawingsService: DoneDrawingsService;
  @inject(DrawingImagesService) public drawingsImageService: DrawingImagesService;
  @inject(NotificationsService) public notificationsService: NotificationsService;


  @httpGet('/', AuthMiddleware)
  async getAll(@request() req: Request) {

    return {
      type: 'success',
      doneDrawings: await this.doneDrawingsService.getAll(req)
    };

  }


  @httpGet('/:id', AuthMiddleware)
  async get(@requestParam('id') id: string, @request() req: Request) {

    const doneDrawing = await this.doneDrawingsService.findById(id, req, true);
    if (!doneDrawing) { throw new DrawingNotFoundError(); }

    return {
      type: 'success',
      doneDrawing
    };

  }


  @httpPost('/top', AuthMiddleware)
  async getTop(@request() req: Request) {

    const date: number = req.body.date;

    if (!date) { throw new MissingDateError(); }

    return {
      type: 'success',
      doneDrawings: await this.doneDrawingsService.getWeeklyTop(new Date(date), req)
    };

  }


  @httpPut('/:id/like', AuthMiddleware)
  async like(@requestParam('id') id: string, @request() req: Request) {

    const doneDrawing = await this.doneDrawingsService.findStoreById(id);
    await this.doneDrawingsService.addLike(this.httpContext.user.details.id, id);

    // Send a notification to the author
    if (doneDrawing.author.toString() !== this.httpContext.user.details.id) {
      this.notificationsService.send(
        doneDrawing.author.toString(),
        'like',
        'Mention j\'aime',
        `${this.httpContext.user.details.username} a aimé votre dessin "${doneDrawing.title}"`,
        'drawing',
        id
      );
    }

    return {
      type: 'success',
      description: 'Mention j\'aime envoyée.',
      doneDrawing: await this.doneDrawingsService.findById(id, req, true)
    };

  }


  @httpDelete('/:id/like', AuthMiddleware)
  async dislike(@requestParam('id') id: string, @request() req: Request) {

    await this.doneDrawingsService.removeLike(this.httpContext.user.details.id, id);

    return {
      type: 'success',
      description: 'Mention j\'aime retirée.',
      doneDrawing: await this.doneDrawingsService.findById(id, req, true)
    };

  }


  @httpPost('/:id/comment', AuthMiddleware)
  async comment(@requestParam('id') id: string, @request() req: Request) {

    const comment: string = req.body.comment;
    if (!comment || !comment.trim()) { throw new MissingCommentError(); }

    const doneDrawing = await this.doneDrawingsService.findStoreById(id);
    await this.doneDrawingsService.addComment(this.httpContext.user.details.id, id, comment.trim());

    // Send a notification to the author
    if (doneDrawing.author.toString() !== this.httpContext.user.details.id) {
      this.notificationsService.send(
        doneDrawing.author.toString(),
        'comment',
        'Commentaire',
        `${this.httpContext.user.details.username} a commenté votre dessin "${doneDrawing.title}"`,
        'drawing',
        id
      );
    }

    return {
      type: 'success',
      description: 'Commentaire envoyé.',
      doneDrawing: await this.doneDrawingsService.findById(id, req, true)
    };

  }


  @httpDelete('/:drawingid/comment/:commentid', AuthMiddleware)
  async removeComment(@requestParam('drawingid') drawingId: string, @requestParam('commentid') commentId: string, @request() req: Request) {

    await this.doneDrawingsService.removeComment(this.httpContext.user.details.id, drawingId, commentId);

    return {
      type: 'success',
      description: 'Commentaire supprimé.',
      doneDrawing: await this.doneDrawingsService.findById(drawingId, req, true)
    };

  }

}
