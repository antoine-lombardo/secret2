# Colorimage Server v1.0.0 - 113

Prototype de serveur pour le projet Colorimage de l'équipe 113. Il n'est pas nécessaire de démarrer le serveur en local, puisque le serveur est hébergé sur Azure. Ce projet n'est utile que pour tester les fonctionnalités du serveur de façon locale, si nécessaire.

## Pré-requis

- [Node v16.13.2](https://nodejs.org/en/download/)
- [npm v8.1.2](https://nodejs.org/en/download/)
- Windows 10

## Installation

Exécuter le fichier _server_install_dependencies.bat_.

**Alternative**

Exécuter la commande `npm ci`.

## Démarrage du serveur

Exécuter le fichier _server_start.bat_.

**Alternative**

Définir les variables d'environnement suivantes:

- `LOGLEVEL` (debug | info)
- `COLORED_LOG` (true | false)
- `ENV` (development | production)
- `MONGO_USER` (poldes)
- `MONGO_PASSWORD` (9fQv57QyN6GTtBHm)
- `MONGO_PATH` (@cluster0.drimb.mongodb.net/polydessin)
- `SECRET` (polydessin-super-secret-key)

Exécuter la commande `node build\index.js`.

## Utilisation du serveur

Le serveur rend disponible un serveur REST API et un serveur Socket-IO. Le point d'accès local est `http://localhost:5000/`.

Les routes REST et les events Socket.IO disponibles sont présentés dans le protocole de communication.
