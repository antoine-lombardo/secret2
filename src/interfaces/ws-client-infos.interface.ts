import { WebSocket } from 'ws';

export interface WSClientInfos {
    uuid: string;
    ws: WebSocket;
    nickname: string;
}

export default WSClientInfos;
