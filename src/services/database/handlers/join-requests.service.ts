import { JoinRequest, JoinRequestStore, User } from '@src/interfaces';
import { AlbumsService } from '@src/services';
import {
    AlbumMemberPermissionError,
    AlbumNotFoundError,
    AlreadyAnsweredRequestError,
    AlreadyHasAccessRequestError,
    AlreadyMemberRequestError,
    AnotherRequestPendingError,
    DatabaseError,
    JoinRequestNotFoundError } from '@src/utils/http-errors';
import { Request } from 'express';
import { inject, injectable, LazyServiceIdentifer } from 'inversify';
import mongoose from 'mongoose';
import {ObjectId} from 'mongoose';
import { JoinRequestModel } from '../models/join-request.model';
import { UsersService } from './users.service';

@injectable()
export class JoinRequestsService {
    usersService: UsersService;
    albumsService: AlbumsService;



    model = JoinRequestModel;



    constructor(
        @inject(new LazyServiceIdentifer(() => UsersService)) usersService: UsersService,
        @inject(new LazyServiceIdentifer(() => AlbumsService)) albumsService: AlbumsService
    ) {
        this.usersService = usersService;
        this.albumsService = albumsService;
    }


    /**
     * Returns if there is an existing pending request from the user for the provided album.
     * @param userId The user ID
     * @param albumId The album ID
     * @return A promise to be true or false
     */
    async pendingRequestExist(userId: string, albumId: string): Promise<boolean> {
        try {
            const entry = await this.model.findOne({user: userId, album: albumId, dateAnswered: { $eq: null }});
            return (entry) ? true : false;
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Returns all pending requests for the provided album
     * @param albumId The album ID
     * @param userId The requesting user ID
     * @param request The request
     * @return A promise to be true or false
     */
    async findPendingForAlbum(albumId: string, userId: string, request: Request): Promise<JoinRequest[]> {
        const album = await this.albumsService.findById(albumId, userId, request);
        if (!album) { throw new AlbumNotFoundError(); }
        if (!album.hasAccess) { throw new AlbumMemberPermissionError(); }
        try {
            const entries = await this.model.find({album: albumId, dateAnswered: { $eq: null }});
            if (!entries) { return []; }
            const joinRequests: JoinRequest[] = [];
            for (const entry of entries) {
                joinRequests.push(await this.storeToDict(entry, userId, request));
            }
            return joinRequests;
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Returns the join request
     * @param requestId The join request ID
     * @param userId The requesting user ID
     * @param request The request
     * @return A promise to be true or false
     */
    async findById(requestId: string, userId: string, request: Request): Promise<JoinRequest> {
        const joinRequest = await this.model.findById(requestId);
        if (!joinRequest) { return null; }
        return this.storeToDict(joinRequest, userId, request);
    }



    /**
     * Accepts or decline a request
     * @param requestId The join request ID
     * @param accepted If request is accepted or not
     * @param userId The user who is accepting the request
     * @param request The request
     * @return A promise to be true or false
     */
    async answer(requestId: string, accepted: boolean, userId: string, request: Request) {
        const joinRequest = await this.findById(requestId, userId, request);
        if (!joinRequest) { throw new JoinRequestNotFoundError(); }
        if (!joinRequest.album.hasAccess) { throw new AlbumMemberPermissionError(); }
        if (joinRequest.dateAnswered) { throw new AlreadyAnsweredRequestError(); }
        for (const member of joinRequest.album.members) {
            if (member.id === joinRequest.user.id) {
                await this.delete(joinRequest.id);
                throw new AlreadyMemberRequestError();
            }
        }
        try {
            await this.model.findByIdAndUpdate(joinRequest.id, {
                dateAnswered: Date.now(),
                accepted
            });
        } catch {
            throw new DatabaseError();
        }
        if (accepted) { await this.albumsService.addMember(joinRequest.album.id, joinRequest.user.id); }
    }




    /**
     * Creates a join request
     * @param userId The user ID of the user willing to join the album
     * @param albumId The album Id that user want to join
     * @return A promise to be the created join request.
     */
    async create(userId: string, albumId: string, request: Request): Promise<JoinRequest> {
        // Check if album exist
        const album = await this.albumsService.findById(albumId, userId, request);
        if (!album) { throw new AlbumNotFoundError(); }

        // Check if already has access
        if (album.hasAccess) { throw new AlreadyHasAccessRequestError(); }

        // Check if another request exist
        if (await this.pendingRequestExist(userId, albumId)) { throw new AnotherRequestPendingError(); }

        // Create request
        const joinRequest = new JoinRequestModel({
            user: userId,
            album: albumId,
            dateCreated: Date.now(),
            dateAnswered: null,
            accepted: false
        });
        try {
            return await this.storeToDict(await joinRequest.save(), userId, request);
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Deleted a join request
     * @param requestId The user ID of the user willing to join the album
     */
    async delete(requestId: string) {
        try {
            this.model.findByIdAndDelete(requestId);
        } catch {
            throw new DatabaseError();
        }
    }



    async storeToDict(joinRequestStore: JoinRequestStore & mongoose.Document, userId: string, request: Request): Promise<JoinRequest> {
        // Populate
        const populatedJoinRequestStore = await joinRequestStore.populate(['user', 'album']);
        // Get details
        const user = this.usersService.getPublicProfileFromUser((populatedJoinRequestStore.user as any)._doc as User, request);
        const album = await this.albumsService.storeToDict(populatedJoinRequestStore.album as any, userId, request);
        // Create Join Request structure
        return {
            id: joinRequestStore.id,
            user,
            album,
            accepted: joinRequestStore.accepted,
            answered: joinRequestStore.dateAnswered ? true : false,
            dateCreated: new Date(joinRequestStore.dateCreated).getTime(),
            dateAnswered: joinRequestStore.dateAnswered ? new Date(joinRequestStore.dateAnswered).getTime() : null,
        };
    }
}

