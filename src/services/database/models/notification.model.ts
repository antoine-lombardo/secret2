import { NotificationStore } from '@src/interfaces';
import mongoose from 'mongoose';

export const NotificationModel = mongoose.model<NotificationStore>(
    'Notification',
    new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        notificationType: String,
        title: String,
        description: String,
        timestamp: Number,
        linkType: String,
        linkData: String
    })
);
