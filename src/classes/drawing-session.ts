import { AuthenticatedSocket, DrawEventMessage, EditMessage, Layer, User } from '@src/interfaces';
import { LayersService, SocketsService, UsersService } from '@src/services';
import { logger } from '@services/logger';
import { LayerCreatedMessage } from '@src/interfaces/socket-responses/layer-created-message..interface';
import { Coordinates } from '@src/interfaces/coordinates.interface';


export class DrawingSession {

    public socketsService: SocketsService;
    public layersService: LayersService;
    public usersService: UsersService;

    editors: {[index: string]: {socket: AuthenticatedSocket; timeJoin: number}} = {};
    layers: {[index: string]: EditMessage} = {};
    updatedLayers = new Set<string>();
    drawingId: string;

    constructor(drawingId: string) {
        this.drawingId = drawingId;
    }

    public async load() {
        for (const layer of await this.layersService.findLayersByDrawing(this.drawingId)) {
            this.layers[layer.id] = this.layerToEditMessage(layer);
        }
        logger.debug(`Loaded drawing "${this.drawingId}" into memory.`);
    }

    public async unload() {
        // Little to only update modified layer on unload
        for (const layerId of this.updatedLayers.values()) {
            const layer = this.layers[layerId];
            if (!layer) { continue; }
            try {
                if (layer.editType === 'delete') {
                    await this.layersService.delete(layer);
                } else {
                    await this.layersService.modify(layer);
                }
            } catch {
                logger.error(`Error while unloading layer "${layer.layerId}".`);
            }
        }
        logger.debug(`Unloaded drawing "${this.drawingId}" from memory.`);
    }

    //
    // Loading
    //

    private layerToEditMessage(layer: Layer): EditMessage {
        return {
            type: 'edit',
            editType: 'modify',
            layerId: layer.id,
            layerType: layer.type,
            creator: layer.creator,
            strokeColor: layer.strokeColor,
            fillColor: layer.fillColor,
            strokeThickness: layer.strokeThickness,
            isStroked: layer.isStroked,
            isFilled: layer.isFilled,
            path: layer.path,
            editor: null,
            editing: false,
            isSelecting: false,
            timestamp: layer.timestamp
        };
    }


    //
    // Editor
    //

    handleJoin(socket: AuthenticatedSocket) {
        // If already in, skip
        if (this.editors[socket.user.id]) { return; }
        // Emit to all
        const joinEvent: DrawEventMessage = {
            type: 'drawEvent',
            eventType: 'collabJoin',
            userId: socket.user.id
        };
        for (const collabInfos of Object.values(this.editors)) {
            collabInfos.socket.emit('PUBLIC_MESSAGE', joinEvent);
        }
        // Add to collaborators
        this.editors[socket.user.id] = {socket, timeJoin: Date.now()};
        logger.debug(`User "${socket.user.username}" joined drawing "${this.drawingId}"`);

    }

    handleQuit(socket: AuthenticatedSocket) {
        // If already out, skip
        if (!this.editors[socket.user.id]) { return; }
        // Emit to all
        const joinEvent: DrawEventMessage = {
            type: 'drawEvent',
            eventType: 'collabLeave',
            userId: socket.user.id
        };
        for (const collabInfos of Object.values(this.editors)) {
            collabInfos.socket.emit('PUBLIC_MESSAGE', joinEvent);
        }
        // Update stats
        this.usersService.updateStats(socket.user.id, this.drawingId, Math.round((Date.now() - this.editors[socket.user.id].timeJoin) / 1000));
        // Remove from collaborators
        delete this.editors[socket.user.id];
        logger.debug(`User "${socket.user.username}" quit drawing "${this.drawingId}"`);
    }


    handleDrawingDeleted(reason: string, message: string) {
        // Emit a force quit to all active collaborators
        const joinEvent: DrawEventMessage = {
            type: 'drawEvent',
            eventType: 'kick',
            reason,
            message
        };
        for (const collabInfos of Object.values(this.editors)) {
            // Update stats
            this.usersService.updateStats(collabInfos.socket.user.id, this.drawingId, Math.round((Date.now() - collabInfos.timeJoin) / 1000));
            // Kick collaborator
            collabInfos.socket.emit('PUBLIC_MESSAGE', joinEvent);
        }
    }


    //
    // Handling
    //

    async addLayer(socket: AuthenticatedSocket, command: EditMessage, callback: (response: LayerCreatedMessage) => void) {
        try {
            // Add layer to database (required to generate an ID)
            const layer = await this.layersService.create(socket.user.id, command, this.drawingId);
            // Fix path if necessary
            let path: Coordinates[];
            if (layer.type === 'ellipse' || layer.type === 'rectangle') {
                if (!layer.path || layer.path.length === 0) { path = [{x: 0, y: 0}, {x: 0, y: 0}]; }
                else if (layer.path.length === 1) { path = [layer.path[0], layer.path[0]]; }
                else { path = [layer.path[0], layer.path[1]]; }
            } else {
                path = layer.path;
            }
            // Send layer ID to the creator
            callback({type: 'created', layerId: layer.id});
            // Generate an emittable message
            const emitMessage: EditMessage = {
                type: 'edit',
                editType: 'add',
                layerId: layer.id,
                layerType: layer.type,
                creator: layer.creator,
                strokeColor: layer.strokeColor,
                fillColor: layer.fillColor,
                strokeThickness: layer.strokeThickness,
                isFilled: layer.isFilled,
                isStroked: layer.isStroked,
                path: layer.path,
                editor: (command.editing === false) ? null : socket.user.id,
                editing: command.editing !== false,
                isSelecting: false,
                timestamp: null
            };
            this.layers[emitMessage.layerId] = emitMessage;
            this.emit(socket.user, emitMessage);
        } catch {
            this.socketsService.logError('Failed to add the shape.', socket);
        }
    }


    async modifyLayer(socket: AuthenticatedSocket, command: EditMessage) {
        try {
            // Ensure we know the layer
            const layer = this.layers[command.layerId];
            if (!layer) { throw Error(); }
            if (layer.editType === 'delete') { throw Error(); }
            // Ensure the layer isn't locked
            if (layer.editing && layer.editor !== socket.user.id) { throw Error(); }
            // Update locally
            layer.editType = 'modify';
            if (command.editing === false || command.editing === true) { layer.editing = command.editing; }
            layer.editor = (layer.editing) ? socket.user.id : null;
            if (layer.editing && !layer.timestamp) { layer.timestamp = Date.now(); }
            if (command.strokeColor) { layer.strokeColor = command.strokeColor; }
            if (command.fillColor) { layer.fillColor = command.fillColor; }
            if (command.strokeThickness) { layer.strokeThickness = command.strokeThickness; }
            if (command.isFilled === true || command.isFilled === false ) { layer.isFilled = command.isFilled ; }
            if (command.isStroked === true || command.isStroked === false ) { layer.isStroked = command.isStroked ; }
            if (layer.editing && (command.isSelecting === true || command.isSelecting === false)) { layer.isSelecting = command.isSelecting; }
            if (!layer.editing ) { layer.isSelecting = false; }
            // Fix path if necessary
            if (command.path) {
                if (layer.type === 'ellipse' || layer.type === 'rectangle') {
                    if (command.path.length === 0) { layer.path = [{x: 0, y: 0}, {x: 0, y: 0}]; }
                    else if (command.path.length === 1) { layer.path = [command.path[0], command.path[0]]; }
                    else { layer.path = [command.path[0], command.path[1]]; }
                } else {
                    layer.path = command.path;
                }
            }
            // Send the updated layer
            await this.emit(socket.user, layer);
        } catch {
            this.socketsService.logError('Failed to modify the shape.', socket);
        }
    }


    async deleteLayer(socket: AuthenticatedSocket, command: EditMessage) {
        try {
            // Ensure we know the layer
            const layer = this.layers[command.layerId];
            if (!layer) { throw Error(); }
            if (layer.editType === 'delete') { throw Error(); }
            // Ensure the layer isn't locked
            if (layer.editing && layer.editor !== socket.user.id) { throw Error(); }
            // Update locally
            layer.editType = 'delete';
            layer.editing = false;
            layer.editor = null;
            layer.timestamp = Date.now();
            layer.isSelecting = false;
            // Send the updated layer
            await this.emit(socket.user, layer);
        } catch {
            this.socketsService.logError('Failed to delete the shape.', socket);
        }
    }


    // Emitting

    async emit(user: User, emitMessage: EditMessage) {
        this.updatedLayers.add(emitMessage.layerId);
        for (const collabInfos of Object.values(this.editors)) {
            if (collabInfos.socket.user.id !== user.id) {
                collabInfos.socket.emit('PUBLIC_MESSAGE', emitMessage);
            }
        }
    }

}
