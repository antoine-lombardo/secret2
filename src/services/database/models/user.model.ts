import { User } from '@src/interfaces';
import mongoose from 'mongoose';

export const UserModel = mongoose.model<User>(
    'User',
    new mongoose.Schema({
        username: {
            type: String,
            unique: true
        },
        email: {
            type: String,
            unique: true
        },
        password: String,
        privateMode: Boolean,
        darkMode: Boolean,
        originalAvatarFilename: String,
        resizedAvatarFilename: String,
        statsCollabsNumber: Number,
        statsTotalTime: Number,
        statsMeanTime: Number,
        statsDrawingsNumber: Number,
        statsDrawings: [String],
        logins: [Number],
        logouts: [Number],
        editions: [{
            drawingId: String,
            timestamp: Number
        }]
    })
);
