export interface PrivateProfile {
    id: string;
    username: string;
    email: string;
    privateMode: boolean;
    darkMode: boolean;
    avatar: {
        original: string;
        resized: string
    };
    stats: {
        drawingsNumber: number;
        meanTime: number;
        totalTime: number;
        collabNumber: number;
        drawingAuthorNumber: number;
        albumMemberNumber: number;
    };
    history: {
        type: string,
        timestamp: number,
        link: string
    }[];
}
