import { DrawingStore } from '@src/interfaces/drawing.interface';
import mongoose from 'mongoose';

export const DrawingModel = mongoose.model<DrawingStore>(
    'Drawing',
    new mongoose.Schema({
        album:          { type: mongoose.Schema.Types.ObjectId, ref: 'Album' },
        title: String,
        author:         { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        dateCreated:      Date,
        dateModified:     Date,
        isExposed:        Boolean,
        password:         String
    })
);
