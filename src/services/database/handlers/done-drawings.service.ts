import { DoneDrawing, DoneDrawingStore, PublicProfile, User, Comment, CommentStore, LikeStore } from '@src/interfaces';
import { CommentCreatorPermissionError, CommentNotFoundError, DatabaseError, DrawingCreatorPermissionError, DrawingNotFoundError, InternalError } from '@src/utils/http-errors';
import { Request } from 'express';
import { inject, injectable, } from 'inversify';
import { UsersService } from './users.service';
import { DrawingsService } from './drawings.service';
import { DoneDrawingModel } from '../models/done-drawing.model';
import { DrawingImagesService } from '@src/services/drawing-images';
import * as mongoose from 'mongoose';

import getDecorators from 'inversify-inject-decorators';
import { container } from '@src/inversify.config';
const {lazyInject} = getDecorators(container);

const LIKE_VALUE = 112;
const COMMENT_VALUE = 53;

@injectable()
export class DoneDrawingsService {
    @lazyInject(DrawingsService) drawingsService: DrawingsService;
    @lazyInject(DrawingImagesService) drawingImagesService: DrawingImagesService;
    @lazyInject(UsersService) usersService: UsersService;


    model = DoneDrawingModel;



    constructor() {
        this.usersService.getUsersDoneDrawingsNumber = async (userId: string) => {
            const drawings = await this.model.find({author: userId});
            return (drawings) ? drawings.length : 0;
        };
    }



    /**
     * Creates a done drawing
     * @return A promise to be the done drawing.
     */
    async create(userId: string, drawingId: string, dataUrl: string, request: Request): Promise<DoneDrawing> {
        // Get drawing
        const drawingStore = await this.drawingsService.findByID(drawingId);

        // Check if drawing is valid to get done
        if (!drawingStore) { throw new DrawingNotFoundError(); }
        if (userId !== drawingStore.author.toString()) { throw new DrawingCreatorPermissionError(); }

        // Save done drawing
        const doneDrawing = new DoneDrawingModel({
            title: drawingStore.title,
            author: drawingStore.author,
            collaborators: [], // TODO
            dateCreated: Date.now(),
            likes: [],
            comments: []
        });
        const savedDoneDrawing = await doneDrawing.save();

        // Try to save the image
        try {
            await this.drawingImagesService.save('done', savedDoneDrawing.id, dataUrl);
            return await this.storeToDict(savedDoneDrawing, request, false);
        } catch (err) {
            await savedDoneDrawing.delete();
            throw err;
        }

    }


    /**
     * Gets all done drawings
     * @return A promise to be all the done drawing.
     */
    async getAll(request: Request): Promise<DoneDrawing[]> {

        try {
            const doneDrawingStores = await this.model.find();
            const doneDrawings = [];
            for (const doneDrawingStore of doneDrawingStores) {
                doneDrawings.push(await this.storeToDict(doneDrawingStore, request, false));
            }
            return doneDrawings;
        } catch {
            throw new DatabaseError();
        }

    }

    /**
     * Gets all done drawings
     * @return A promise to be all the done drawing.
     */
    async getWeeklyTop(date: Date, request: Request): Promise<DoneDrawing[]> {

        const interval = this.getWeekInterval(date);
        console.log(interval);

        try {
            const doneDrawingStores = await this.model.find({ dateCreated: { $gte: interval[0], $lt: interval[1] } });
            const doneDrawings = [];
            for (const doneDrawingStore of doneDrawingStores) {
                doneDrawings.push(await this.storeToDict(doneDrawingStore, request, true));
            }
            doneDrawings.sort((a: DoneDrawing, b: DoneDrawing) => {
                if (a.score < b.score) {
                    return -1;
                }
                else if (a.score > b.score) {
                    return 1;
                }
                else {
                    if (a.dateCreated > b.dateCreated) {
                        return -1;
                    }
                    else if (a.dateCreated < b.dateCreated) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            });
            doneDrawings.reverse();
            const topDrawings = doneDrawings.slice(0, 10);
            return topDrawings;
        } catch {
            throw new InternalError();
        }

    }



    async addLike(userId: string, doneDrawingId: string) {

        const doneDrawing = await this.findStoreById(doneDrawingId);
        await doneDrawing.populate('likes');
        for (const like of doneDrawing.likes) {
            if (like.user.toString() === userId && !like.hidden) { return; }
        }
        await doneDrawing.updateOne({ $addToSet: { likes: {user: userId, timestamp: Date.now(), hidden: false} } });
        await doneDrawing.save();

    }

    async removeLike(userId: string, doneDrawingId: string) {

        const doneDrawing = await this.findStoreById(doneDrawingId);
        let likeStore: LikeStore = null;
        for (const like of doneDrawing.likes) {
            if (like.user.toString() === userId && !like.hidden) {
                likeStore = like;
            }
        }
        if (!likeStore) { return; }
        await doneDrawing.updateOne({ $pull: { 'likes': {_id: likeStore.id} } });
        if (this.isUntouchable(likeStore.timestamp)) {
            await doneDrawing.updateOne({ $push: { likes: {
                user: likeStore.user,
                hidden: true,
                timestamp: likeStore.timestamp
            }}});
        }
        await doneDrawing.save();

    }

    async addComment(userId: string, doneDrawingId: string, comment: string) {

        await this.findStoreById(doneDrawingId);
        await this.model.findByIdAndUpdate(doneDrawingId, { $push: { comments: {
            author: userId,
            comment,
            hidden: false,
            timestamp: Date.now()
        } } } );

    }

    async removeComment(userId: string, doneDrawingId: string, commentId: string) {

        const doneDrawing = await this.findStoreById(doneDrawingId);
        await doneDrawing.populate('comments');
        let commentStore: CommentStore = null;
        for (const comment of doneDrawing.comments) {
            if (comment.id === commentId) {
                commentStore = comment;
                if (comment.author.toString() !== userId) {
                    throw new CommentCreatorPermissionError();
                }
            }
        }
        if (!commentStore) { throw new CommentNotFoundError(); }

        await doneDrawing.updateOne({ $pull: { comments: {_id: commentId} } });
        if (this.isUntouchable(commentStore.timestamp)) {
            await doneDrawing.updateOne({ $push: { comments: {
                author: commentStore.author,
                comment: commentStore.comment,
                hidden: true,
                timestamp: commentStore.timestamp
            }}});
        }
        await doneDrawing.save();

    }

    async calculateScore(doneDrawingStore: DoneDrawingStore & mongoose.Document): Promise<number> {
        const populatedDoneDrawingStore = await doneDrawingStore.populate(['likes', 'comments']);
        const interval = this.getWeekInterval(new Date(doneDrawingStore.dateCreated));
        // filter likes
        let nLikes = 0;
        for (const like of populatedDoneDrawingStore.likes) {
            if (like.timestamp >= interval[0] &&  like.timestamp < interval[1]) {
                nLikes += 1;
            }
        }
        // filter comments
        let nComments = 0;
        for (const comment of populatedDoneDrawingStore.comments) {
            if (comment.timestamp >= interval[0] &&  comment.timestamp < interval[1]) {
                nComments += 1;
            }
        }
        return nLikes * LIKE_VALUE + nComments * COMMENT_VALUE;

    }

    async findStoreById(doneDrawingId: string): Promise<DoneDrawingStore & mongoose.Document> {

        let doneDrawing: DoneDrawingStore & mongoose.Document;
        try {
            doneDrawing = await this.model.findById(doneDrawingId);
        } catch {
            throw new DrawingNotFoundError();
        }
        if (!doneDrawing) { throw new DrawingNotFoundError(); }
        return doneDrawing;

    }


    async findById(doneDrawingId: string, request: Request, calculateScore: boolean): Promise<DoneDrawing> {

        const doneDrawingStore = await this.findStoreById(doneDrawingId);
        return await this.storeToDict(doneDrawingStore, request, calculateScore);

    }



    async storeToDict(doneDrawingStore: DoneDrawingStore & mongoose.Document, request: Request, calculateScore: boolean): Promise<DoneDrawing> {

        // Calculate score
        let score: number | null = null;
        if (calculateScore) { score = await this.calculateScore(doneDrawingStore); }

        // Populate
        const populatedDoneDrawingStore = await doneDrawingStore.populate(['author', 'collaborators', 'likes', 'comments', {path: 'likes', populate: { path:  'user', model: 'User' }}]);

        // Get public profiles
        const author = this.usersService.getPublicProfileFromUser((populatedDoneDrawingStore.author as any)._doc as User, request);
        const collaborators: PublicProfile[] = [];
        for (const collaborator of populatedDoneDrawingStore.collaborators) {
            collaborators.push(this.usersService.getPublicProfileFromUser((collaborator as any)._doc as User, request));
        }
        const likes: PublicProfile[] = [];
        for (const like of populatedDoneDrawingStore.likes) {
            if (!like.hidden) {
                likes.push(this.usersService.getPublicProfileFromUser((like.user as any)._doc as User, request));
            }
        }
        // Get images
        const images = this.drawingImagesService.getUrls('done', doneDrawingStore.id, request);
        // Comments
        const comments: Comment[] = [];
        for (const comment of populatedDoneDrawingStore.comments) {
            if (!comment.hidden) {
                comments.push({
                    id: comment.id,
                    timestamp: comment.timestamp,
                    comment: comment.comment,
                    author: await this.usersService.getPublicProfile(comment.author.toString(), request)
                });
            }
        }
        // Create done drawing structure
        return {
            id: doneDrawingStore.id,
            title: doneDrawingStore.title,
            author,
            collaborators,
            dateCreated: doneDrawingStore.dateCreated,
            likes,
            comments,
            original: (images) ? images.original : null,
            thumbnail: (images) ? images.thumbnail : null,
            score
        };

    }


    private isUntouchable(date: number): boolean {
        const interval = this.getWeekInterval(new Date());
        if (date >= interval[0]) { return false; }
        return true;
    }



    private getPreviousMonday(date: Date): Date {
        const previousMonday = new Date(date);
        const day = previousMonday.getDay() || 7;
        if ( day !== 1 ) { previousMonday.setDate(previousMonday.getDate() - day + 1); }
        previousMonday.setHours(0, 0, 0, 0);
        return previousMonday;
    }



    private getWeekInterval(date: Date): number[] {
        const previousMonday = this.getPreviousMonday(date);
        const upcomingSunday = new Date(previousMonday.getTime() + (7 * 1000 * 60 * 60 * 24));
        return [previousMonday.getTime(), upcomingSunday.getTime()];
    }

}

