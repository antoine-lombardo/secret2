import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Avatar

export class NoFileProvidedError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'NO_FILE_PROVIDED', 'Aucun fichier n\'a été fourni.');
    }
}

export class TooManyFileProvidedError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'TOO_MANY_FILE_PROVIDED', 'Trop de fichiers ont été fournis.');
    }
}

export class FileTooBigError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'FILE_TOO_BIG', 'Fichier trop lourd.');
    }
}

export class FileTypeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'FILE_TYPE', 'Type de fichier non pris en charge.');
    }
}

export class FileCorruptError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'FILE_CORRUPT', 'Fichier corrompu.');
    }
}

export class ImageTooLargeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'IMAGE_TOO_LARGE', 'Résolution de l\ìmage trop grande.');
    }
}

export class ImageTooSmallError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'IMAGE_TOO_SMALL', 'Résolution de l\ìmage trop petite.');
    }
}

export class AvatarNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'AVATAR_NOT_FOUND', 'Impossible de récupérer l\'avatar sur le serveur.');
    }
}


export class MissingPasswordChangedIndicatorError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_PASSWORD_CHANGED_INDICATOR', 'L\'indicateur de mot de passe modifié est manquant.');
    }
}


export class MissingAvatarChangedIndicatorError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_AVATAR_CHANGED_INDICATOR', 'L\'indicateur d\'avatar modifié est manquant.');
    }
}


export class MissingPrivateModeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_PRIVATE_MODE', 'Le niveau de confidentialité du compte est manquant.');
    }
}


export class MissingDarkModeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_DARK_MODE', 'Le mode d\'affichage du compte est manquant.');
    }
}


export class UnknownAvatarTypeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'UNKNOWN_AVATAR_TYPE', 'Type d\'envoi d\'avatar inconnu.');
    }
}


export class MissingAvatarError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_AVATAR', 'Avatar manquant.');
    }
}


export class InvalidAvatarDataError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'INVALID_AVATAR_DATA_ERROR', 'Impossible de décoder l\'avatar.');
    }
}


export class InvalidAvatarTypeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'INVALID_AVATAR_TYPE_ERROR', 'Type de fichier d\'avatar invalide.');
    }
}
