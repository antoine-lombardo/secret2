import { ObjectId } from 'mongoose';
import { Coordinates } from './coordinates.interface';
import { PublicProfile } from './public-profile.interface';

export interface LayerStore {
    _id: ObjectId;
    id: string;
    type: string;
    creator: ObjectId;
    strokeColor: string;
    fillColor: string;
    strokeThickness: number;
    isFilled: boolean;
    isStroked: boolean;
    drawing: ObjectId;
    timestamp: number;
    path: Coordinates[];
    valid: boolean;
}

export interface Layer {
    id: string;
    type: string;
    creator: string;
    strokeColor: string;
    fillColor: string;
    strokeThickness: number;
    isFilled: boolean;
    isStroked: boolean;
    drawing: string;
    timestamp: number;
    path: Coordinates[];
    valid: boolean;
}
