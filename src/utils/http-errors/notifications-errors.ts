import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

export class MissingNotificationTypeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_NOTIFICATION_TYPE', 'Type de notification manquant.');
    }
}

export class MissingNotificationLinkTypeError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_NOTIFICATION_LINK_TYPE', 'Type de lien de notification manquant.');
    }
}

export class MissingNotificationLinkDataError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_NOTIFICATION_LINK_DATA', 'Données de lien de notification manquant.');
    }
}

export class MissingNotificationTitleError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_NOTIFICATION_TITLE', 'Titre de la notification manquant.');
    }
}

export class MissingNotificationDescriptionError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_NOTIFICATION_DESCRIPTION', 'Description de la notification manquante.');
    }
}

export class NotificationAccessError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'NOTIFICATION_ACCES_ERROR', 'Vous ne pouvez que gérer vos propres notifications.');
    }
}


export class NotificationNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'NOTIFICATION_NOT_FOUND', 'Notification introuvable.');
    }
}
