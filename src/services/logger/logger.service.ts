import { config, isDevelopment } from '@src/config';
import { Logger, createLogger, format , transports} from 'winston';

export let logger: Logger;

export const initLogger = () => {
    logger = createLogger({
        format: format.combine(
            format.cli(),
            (config.ColoredLog) ? format.colorize() : format.uncolorize()
        ),
        level: config.LogLevel,
        // transports: [
        //    new transports.File({
        //        level: config.LogLevel,
        //        filename: `${config.LogDir}/server.log`,
        //        handleExceptions: true,
        //        maxsize: config.LogSize * 1024 * 1024,
        //        maxFiles: 5
        //    }),
        //    new transports.File({
        //        level: 'error',
        //        filename: `${config.LogDir}/error.log`,
        //        handleExceptions: true,
        //        maxsize: config.LogSize * 1024 * 1024,
        //        maxFiles: 5
        //    })
        // ],
    });

    const consoleTransport = new transports.Console();
    logger.add(consoleTransport);
};

export default logger;
