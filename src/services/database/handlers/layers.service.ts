import { Album, AlbumStore, EditMessage, Layer, LayerStore, PublicProfile, User } from '@src/interfaces';
import { Coordinates } from '@src/interfaces/coordinates.interface';
import { AlbumModel } from '@src/services/database/models';
import { AlbumCreatorPermissionError, AlbumNotFoundError, DatabaseError } from '@src/utils/http-errors';
import { Request } from 'express';
import mongoose from 'mongoose';
import { inject, injectable } from 'inversify';
import { LayerModel } from '../models/layer.model';
import { UsersService } from './users.service';
import { LayerLockedError, LayerNotFound, NoLayerIDError } from '@src/utils/http-errors/layers-errors';

@injectable()
export class LayersService {
    @inject(UsersService) usersService: UsersService;
    model = LayerModel;
    constructor() {}



    /**
     * Creates a layer
     * @param userID The user ID of the user creating the layer.
     * @param editMessage The edit message
     * @return A promise to be the created layer
     */
    async create(userId: string, editMessage: EditMessage, drawingId: string): Promise<Layer> {
        const layerModel = new LayerModel({
            type: editMessage.layerType,
            creator: userId,
            currentEditor: userId,
            strokeColor: editMessage.strokeColor,
            fillColor: editMessage.fillColor,
            strokeThickness: editMessage.strokeThickness,
            isFilled: editMessage.isFilled ? true : false,
            isStroked: editMessage.isStroked ? true : false,
            drawing: drawingId,
            timestamp: null,
            path: editMessage.path,
            valid: false,
        });
        try {
            const savedLayer = await layerModel.save();
            const layer = await this.storeToDict(savedLayer);
            return layer;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Modifes a layer
     * @param userID The user ID of the user creating the layer.
     * @param editMessage The edit message
     */
    async modify(editMessage: EditMessage) {
        // Build filter
        const filter: {[index: string]: any} = {};
        if (editMessage.strokeColor) { filter['strokeColor'] = editMessage.strokeColor; }
        if (editMessage.fillColor) { filter['fillColor'] = editMessage.fillColor; }
        if (editMessage.isFilled === true || editMessage.isFilled === false) { filter['isFilled'] = editMessage.isFilled; }
        if (editMessage.isStroked === true || editMessage.isStroked === false) { filter['isStroked'] = editMessage.isStroked; }
        if (editMessage.strokeThickness) { filter['strokeThickness'] = editMessage.strokeThickness; }
        if (editMessage.path) { filter['path'] = editMessage.path; }
        if (!editMessage.timestamp) { filter['timestamp'] = Date.now(); }
        else { filter['timestamp'] = editMessage.timestamp; }
        filter['valid'] = true;

        try {
            await this.model.findByIdAndUpdate(editMessage.layerId, filter);
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Deletes a layer
     * @param editMessage The edit message
     */
    async delete(editMessage: EditMessage) {
        try {
            await this.model.findByIdAndDelete(editMessage.layerId);
        } catch {
            throw new DatabaseError();
        }
    }



    async findLayersByDrawing(drawingId: string): Promise<Layer[]> {
        const layerStores = await this.model.find({drawing: drawingId});
        const layers: Layer[] = [];
        for (const layerStore of layerStores) { layers.push(await this.storeToDict(layerStore)); }
        return layers;
    }



    async deleteDrawingLayers(drawingId: string) {
        await this.model.deleteMany({drawing: drawingId});
    }



    private async storeToDict(layerStore: LayerStore & mongoose.Document): Promise<Layer> {
        return {
            id: layerStore.id,
            type: layerStore.type,
            creator: layerStore.creator.toString(),
            strokeColor: layerStore.strokeColor,
            fillColor: layerStore.fillColor,
            strokeThickness: layerStore.strokeThickness,
            isFilled: layerStore.isFilled,
            isStroked: layerStore.isStroked,
            path: layerStore.path,
            timestamp: layerStore.timestamp,
            drawing: layerStore.drawing.toString(),
            valid: layerStore.valid
        };
    }



}

