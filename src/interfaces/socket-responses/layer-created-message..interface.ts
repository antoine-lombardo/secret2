import { SocketMessage } from '../socket-message.interface';

export interface LayerCreatedMessage extends SocketMessage {
    type: string;
    layerId: string;
}
