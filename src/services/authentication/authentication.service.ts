import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { config } from '@src/config';
import { injectable } from 'inversify';
import { TokenPayload, User } from '@src/interfaces';


@injectable()
export class AuthenticationService {



    constructor() {}



    /**
     * Validates the password for the provided user.
     * @param user The user to validate password from.
     * @param password The password to validate.
     * @return Either true or false.
     */
    checkPassword(user: User, password: string): boolean {
        return bcrypt.compareSync(password, user.password);
    }



    /**
     * Hashed a password.
     * @param password The password to validate.
     * @return The hashed password
     */
    hashPassword(password: string): string {
        return bcrypt.hashSync(password, 8);
    }



    /**
     * Generate an access token for the provided user.
     * @param user The user.
     * @return A promise to be the access token.
     */
    async generateToken(user: User): Promise<string> {
        const payload: TokenPayload = { id: user.id };
        const token = jwt.sign(payload, config.Secret, { expiresIn: 15552000 });
        return token;
    }



    /**
     * Validate the access token
     * @param accessToken The access token.
     * @return A promise to be either the user id or null if token is invalid.
     */
    validateToken(accessToken: string): TokenPayload {
        try {
            const payload = jwt.verify(accessToken, config.Secret) as TokenPayload;
            if (!payload.id) { return null; }
            return payload;
        }
        catch {
            return null;
        }
    }



}

