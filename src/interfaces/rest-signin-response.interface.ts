import { RestMessage } from './rest-error.interface';

export interface RestSigninResponse extends RestMessage {
    authInfos: {
        id: string,
        username: string,
        email: string,
        accessToken: string
    };
}

export default RestSigninResponse;
