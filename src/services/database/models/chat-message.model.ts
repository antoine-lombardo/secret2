import { AlbumStore, ChatMessageStore } from '@src/interfaces';
import mongoose from 'mongoose';

export const ChatMessageModel = mongoose.model<ChatMessageStore>(
    'ChatMessage',
    new mongoose.Schema({
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        room: {type: mongoose.Schema.Types.ObjectId, ref: 'ChatRoom'},
        message: String,
        timestamp: Number
    })
);
