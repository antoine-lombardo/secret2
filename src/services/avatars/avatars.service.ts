import { injectable } from 'inversify';
import { config } from '@src/config';
import * as fs from 'fs';
import * as path from 'path';
import { Avatar, PresetAvatar } from '@src/interfaces';
import { Request } from 'express';
import { formatHostname } from '@src/utils/rest-utils';
import moment from 'moment';
import { FileCorruptError, ImageTooLargeError, ImageTooSmallError, InternalError, InvalidAvatarDataError, InvalidAvatarTypeError } from '@src/utils/http-errors';
import sharp from 'sharp';
import { imageSize } from 'image-size';

const AVATARS_DIR = path.normalize(config.FilesDir + '/avatars');
const AVATAR_MAX_WIDTH = 1000;
const AVATAR_MAX_HEIGHT = 1000;
const AVATAR_MIN_WIDTH = 32;
const AVATAR_MIN_HEIGHT = 32;
const AVATAR_RESIZE_WIDTH = 64;
const AVATAR_RESIZE_HEIGHT = 64;

@injectable()
export class AvatarsService {

    constructor() {}

    getPresetAvatarFilenames(): PresetAvatar[] {
        const presetDirs = fs.readdirSync(`${AVATARS_DIR}/base`);
        const filenames: PresetAvatar[] = [];
        for (const preset of presetDirs) {
            if (fs.existsSync(`${AVATARS_DIR}/base/${preset}/original.png`) && fs.existsSync(`${AVATARS_DIR}/base/${preset}/resized.png`)) {
                filenames.push({
                    name: preset,
                    original: `/base/${preset}/original.png`,
                    resized: `/base/${preset}/resized.png`,
                });
            }
        }
        return filenames;
    }

    getPresetAvatarFilenameUrls(req: Request): PresetAvatar[] {
        const filenames = this.getPresetAvatarFilenames();
        const urls: PresetAvatar[] = [];
        for (const filename of filenames) {
            urls.push({
                name: filename.name,
                original: `${formatHostname(req)}/files/avatars${filename.original}`,
                resized: `${formatHostname(req)}/files/avatars${filename.resized}`
            });
        }
        return urls;
    }

    getPresetAvatarFilename(name: string): PresetAvatar | null {
        if (!fs.existsSync(`${AVATARS_DIR}/base/${name}/original.png`) || !fs.existsSync(`${AVATARS_DIR}/base/${name}/resized.png`)) {
            return null;
        }
        return {
            name: name,
            original: `/base/${name}/original.png`,
            resized: `/base/${name}/resized.png`
        };
    }

    getPresetAvatarUrl(name: string, req: Request): PresetAvatar | null {
        const filename = this.getPresetAvatarFilename(name);
        if (!filename) { return null; }
        return {
            name: name,
            original: `${formatHostname(req)}/files/avatars${filename.original}`,
            resized: `${formatHostname(req)}/files/avatars${filename.original}`
        };
    }

    getUserDir(userId: string) {
        return `/${userId}`;
    }

    getUserURL(userId: string, req: Request): string | null {
        const userDir = this.getUserDir(userId);
        if (!userDir) { return null; }
        return `${formatHostname(req)}/files/avatars${userDir}`;
    }

    /**
     * Saves the provided avatar to users avatar directory
     */
    async saveAvatar(userId: string, dataUrl: string): Promise<Avatar> {
        // Parse data url
        const regex = /^data:.+\/(.+);base64,(.*)$/;
        dataUrl = dataUrl.replace(/(\r\n|\n|\r)/gm, '');
        const matches = dataUrl.match(regex);
        if (!matches || matches.length < 2) { throw new InvalidAvatarDataError(); }
        let ext = matches[1];
        const data = matches[2];
        if (ext === 'jpeg') { ext = 'jpg'; }
        if (ext === 'pneg') { ext = 'png'; }
        if (ext !== 'png' && ext !== 'jpg') { throw new InvalidAvatarTypeError(); }

        // Make directory
        const userPath = this.getUserDir(userId);
        if (!fs.existsSync(`${AVATARS_DIR}${userPath}`)) { fs.mkdirSync(`${AVATARS_DIR}${userPath}`); }
        const avatarPath = `${userPath}/${moment().format('YYYYMMDDHHmmss')}`;
        if (!fs.existsSync(`${AVATARS_DIR}${avatarPath}`)) { fs.mkdirSync(`${AVATARS_DIR}${avatarPath}`); }
        const originalPath = `${avatarPath}/original.${ext}`;
        const resizedPath = `${avatarPath}/resized.png`;
        if (fs.existsSync(`${AVATARS_DIR}${originalPath}`)) { fs.unlinkSync(`${AVATARS_DIR}${originalPath}`); }
        if (fs.existsSync(`${AVATARS_DIR}${resizedPath}`)) { fs.unlinkSync(`${AVATARS_DIR}${resizedPath}`); }

        // Save original
        const buffer = Buffer.from(data, 'base64');
        fs.writeFileSync(`${AVATARS_DIR}${originalPath}`, buffer);
        if (!fs.existsSync(`${AVATARS_DIR}${originalPath}`)) { throw new InternalError(); }

        // Validate avatar
        let size: {width: number; height: number} = null;
        try {
            size = imageSize(`${AVATARS_DIR}${originalPath}`);
            if (!size) { throw new Error(); }
        } catch {
            throw new FileCorruptError();
        }
        if (size.height > AVATAR_MAX_HEIGHT || size.width > AVATAR_MAX_WIDTH) {
            throw new ImageTooLargeError();
        }
        else if (size.height < AVATAR_MIN_HEIGHT || size.width < AVATAR_MIN_WIDTH) {
            throw new ImageTooSmallError();
        }

        // Make resized
        await this.resize(`${AVATARS_DIR}${originalPath}`, `${AVATARS_DIR}${resizedPath}`, AVATAR_RESIZE_WIDTH, AVATAR_RESIZE_HEIGHT);
        if (!fs.existsSync(`${AVATARS_DIR}${originalPath}`)) { throw new InternalError(); }

        // Return paths
        return {
            original: originalPath,
            resized: resizedPath
        };


    }

    private async resize(input: string, output: string, width: number, height: number) {
    if (fs.existsSync(output)) { fs.unlinkSync(output); }
    await sharp(input)
      .resize(width, height)
      .png()
      .toFile(output);
  }

}
