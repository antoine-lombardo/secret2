import { InvalidAccessTokenError } from '@src/utils/http-errors';
import { Request, Response, NextFunction } from 'express';
import { injectable } from 'inversify';
import { BaseMiddleware } from 'inversify-express-utils';

@injectable()
export class AuthMiddleware extends BaseMiddleware {

  public handler(request: Request, response: Response, next: NextFunction) {
    if (!this.httpContext.user.details) {
      throw new InvalidAccessTokenError();
    }
    next();
  }

}
