import { DatabaseError, DrawingNotFoundError } from '@src/utils/http-errors';
import { inject, injectable, } from 'inversify';
import mongoose from 'mongoose';
import { UsersService } from './users.service';
import { DrawingsService } from './drawings.service';
import { ChatRoom, ChatRoomStore } from '@src/interfaces/chat-room.interface';
import { ChatRoomModel } from '../models/chat-room.model';
import { CannotDeleteDrawingRoomError, CannotDeletePublicRoomError, ChatRoomAuthorPermissionError, ChatRoomNotFoundError } from '@src/utils/http-errors/chat-errors';
import { ChatMessageModel } from '../models/chat-message.model';
import { ChatMessage, ChatMessageStore } from '@src/interfaces';

@injectable()
export class ChatMessagesService {
    @inject(UsersService) usersService: UsersService;
    @inject(DrawingsService) drawingsService: DrawingsService;


    model = ChatMessageModel;



    constructor() {}



    /**
     * Adds a message to a chat room
     * @return A promise to be the created chat message
     */
    async addMessage(userId: string, chatRoomId: string, message: string): Promise<ChatMessage> {
        const messageModel = new ChatMessageModel({
            user: userId,
            room: chatRoomId,
            message: message,
            timestamp: Date.now()
        });
        try {
            const savedMessage = await messageModel.save();
            const layer = this.storeToDict(savedMessage);
            return layer;
        } catch {
            throw new DatabaseError();
        }
    }


    /**
     * Gets all message from a chat room
     * @return A promise to be the messages
     */
    async findByRoomId(chatRoomId: string): Promise<ChatMessage[]> {
        const messageStores = await this.model.find({room: chatRoomId});
        const messages: ChatMessage[] = [];
        for (const messageStore of messageStores) { messages.push(this.storeToDict(messageStore)); }
        return messages;
    }



    /**
     * Delete all messages from a room
     * @return A promise to be true
     */
    async deleteRoom(chatRoomId: string): Promise<boolean> {
        const messageStores = await this.model.find({room: chatRoomId});
        for (const messageStore of messageStores) { await messageStore.delete(); }
        return true;
    }



    storeToDict(chatMessageStore: ChatMessageStore & mongoose.Document): ChatMessage {
        if (!chatMessageStore) { return null; }
        // Create chat room structure
        return {
            id: chatMessageStore.id,
            userId: chatMessageStore.user.toString(),
            roomId: chatMessageStore.room.toString(),
            message: chatMessageStore.message,
            timestamp: chatMessageStore.timestamp
        };
    }
}

