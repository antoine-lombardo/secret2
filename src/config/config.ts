import * as path from 'path';

// Organize config
export const config = {
    MongoUser: process.env.MONGO_USER ?? '',
    MongoPassword: process.env.MONGO_PASSWORD ?? '',
    MongoPath: process.env.MONGO_PATH ?? '',
    Port: Number(process.env.PORT) || 5000,
    Secret: process.env.SECRET || 'change_me_',
    LogLevel: process.env.LOGLEVEL || 'info',
    LogDir: process.env.LOGDIR || './log',
    LogSize: Number(process.env.LOGSIZE) || 5, // in MB
    Environment: process.env.ENV || 'production',
    ColoredLog: process.env.COLORED_LOG?.toLowerCase() === 'true',
    FilesDir: process.env.FILES_DIR || path.normalize(__dirname + '../../../files')
};

export const isProduction = config.Environment === 'production';
export const isDevelopment = config.Environment === 'development';
