export interface Avatar {
    original: string;
    resized: string;
}
