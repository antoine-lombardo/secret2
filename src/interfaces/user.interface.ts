import { ObjectId } from 'mongoose';

export interface User {
    _id: ObjectId;
    id: string;
    username: string;
    email: string;
    password: string;
    privateMode: boolean;
    darkMode: boolean;
    originalAvatarFilename: string;
    resizedAvatarFilename: string;
    statsCollabsNumber: number;
    statsTotalTime: number;
    statsMeanTime: number;
    statsDrawingsNumber: number;
    statsDrawings: string[];
    logins: number[];
    logouts: number[];
    editions: {
        drawingId: string,
        timestamp: number
    }[];
}

export default User;
