import { ObjectId } from 'mongoose';

export interface ChatRoomStore {
    _id: ObjectId;
    id: string;
    title: string;
    creator: ObjectId;
    isPublicRoom: boolean;
    isDrawingRoom: boolean;
    drawing: ObjectId;
    password: string;
}

export interface ChatRoom {
    id: string;
    title: string;
    creatorId: string;
    isPublicRoom: boolean;
    isDrawingRoom: boolean;
    drawingId: string;
    hasPassword: boolean;
}
