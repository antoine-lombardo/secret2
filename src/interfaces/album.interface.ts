import { ObjectId } from 'mongoose';
import { PublicProfile } from './public-profile.interface';

export interface AlbumStore {
    _id: ObjectId;
    id: string;
    name: string;
    creator: ObjectId;
    description: string;
    private: boolean;
    members: [ObjectId];
}

export interface Album {
    id: string;
    name: string;
    creator: PublicProfile;
    description: string;
    private: boolean;
    hasAccess: boolean;
    members: PublicProfile[];
    hasExposition: boolean;
}
