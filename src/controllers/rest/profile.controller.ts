import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpGet, httpPatch } from 'inversify-express-utils';
import * as fs from 'fs';
import * as path from 'path';
import { config } from '@src/config';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { AvatarsService, UsersService } from '@src/services';
import * as HttpErrors from '@src/utils/http-errors';



const TEMP_DIR = path.normalize(config.FilesDir + '/../temp');
const AVATARS_DIR = path.normalize(config.FilesDir + '/avatars');


@controller('/profile')
export class ProfileController extends BaseHttpController {

  @inject(UsersService) private usersService: UsersService;
  @inject(AvatarsService) private avatarsService: AvatarsService;


  constructor() {
    super();
    // Create directories
    if (!fs.existsSync(TEMP_DIR)) { fs.mkdirSync(TEMP_DIR, { recursive: true }); }
    if (!fs.existsSync(AVATARS_DIR)) { fs.mkdirSync(AVATARS_DIR, { recursive: true }); }
  }



  @httpGet('/', AuthMiddleware)
  async getProfile(request: Request) {
    return {
      type: 'success',
      privateProfile: await this.usersService.getPrivateProfile(this.httpContext.user.details.id, request)
    };

  }


  @httpPatch('/', AuthMiddleware)
  async modifyProfile(req: Request) {
    const avatarChanged: boolean = req.body.avatarChanged;
    const avatarType: string = req.body.avatarType;
    const avatarValue: string = req.body.avatarValue;
    const privateMode: boolean = req.body.privateMode;
    let darkMode: boolean = req.body.darkMode;

    const username: string = req.body.username;
    const email: string = req.body.email;
    let password: string = req.body.password;
    const passwordChanged: boolean = req.body.passwordChanged;

    // Missing avatar params
    if (avatarChanged !== true && avatarChanged !== false) { throw new HttpErrors.MissingAvatarChangedIndicatorError(); }
    if (privateMode !== true && privateMode !== false) { throw new HttpErrors.MissingPrivateModeError(); }
    if (darkMode !== true && darkMode !== false) { darkMode = null; }
    if (avatarChanged) {
      if (avatarType !== 'upload' && avatarType !== 'preset') { throw new HttpErrors.UnknownAvatarTypeError(); }
      if (!avatarValue) { throw new HttpErrors.MissingAvatarError(); }
    }

    // Missing profile params
    if (!username || !username.trim()) { throw new HttpErrors.MissingUsernameError(); }
    if (!email || !email.trim()) { throw new HttpErrors.MissingEmailError(); }
    if (passwordChanged !== true && passwordChanged !== false) { throw new HttpErrors.MissingPasswordChangedIndicatorError(); }
    if (!passwordChanged) { password = null; }
    if (passwordChanged && !password) { throw new HttpErrors.MissingPasswordError(); }

    // Invalid profile params
    if (username.trim() !== this.httpContext.user.details.username) {
      if (await this.usersService.usernameExists(username.trim())) {
        throw new HttpErrors.UsernameAlreadyExistError();
      }
    }
    if (email.trim() !== this.httpContext.user.details.email) {
      if (await this.usersService.emailExists(email.trim())) {
        throw new HttpErrors.EmailAlreadyUsedError();
      }
    }

    if (avatarChanged && avatarType === 'preset') { await this.usersService.setPresetAvatar(this.httpContext.user.details.id, avatarValue); }
    if (avatarChanged && avatarType === 'upload') {
      const avatar = await this.avatarsService.saveAvatar(this.httpContext.user.details.id, avatarValue);
      await this.usersService.setAvatarFilenames(this.httpContext.user.details.id, avatar.original, avatar.resized);
    }

    return {
      type: 'success',
      description: 'Profile modifié avec succès.',
      privateProfile: await this.usersService.modifyUser(this.httpContext.user.details.id, username.trim(), email.trim(), password, privateMode, darkMode, req)
    };

  }



}
