import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Missing Fields

export class LayerNotFound extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'LAYER_NOT_FOUND', 'Impossible de trouber la couche.');
    }
}

export class LayerLockedError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'LAYER_LOCKED', 'La couche est en cours d\'édition par un autre utilisayeur.');
    }
}

export class NoLayerIDError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'NO_LAYER_ID', 'Aucun numéro de couche fourni.');
    }
}
