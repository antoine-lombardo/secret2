import { ObjectId } from 'mongoose';
import { PublicProfile } from './public-profile.interface';
import { Comment, CommentStore } from './comment.interface';
import { LikeStore } from './like.interface';

export interface DoneDrawingStore {
    _id: ObjectId;
    id: string;
    title: string;
    author: ObjectId;
    collaborators: ObjectId[];
    dateCreated: number;
    likes: LikeStore[];
    comments: CommentStore[];
}

export interface DoneDrawing {
    id: string;
    title: string;
    author: PublicProfile;
    collaborators: PublicProfile[];
    dateCreated: number;
    likes: PublicProfile[];
    comments: Comment[];
    thumbnail: string;
    original: string;
    score: number | null;
}
