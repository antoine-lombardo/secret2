import { JoinRequestStore } from '@src/interfaces';
import mongoose from 'mongoose';

export const JoinRequestModel = mongoose.model<JoinRequestStore>(
    'JoinRequest',
    new mongoose.Schema({
        album: {type: mongoose.Schema.Types.ObjectId, ref: 'Album'},
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        dateCreated: Date,
        dateAnswered: Date,
        accepted: Boolean
    })
);
