import {
    DatabaseError,
    MissingNotificationDescriptionError,
    MissingNotificationLinkDataError,
    MissingNotificationLinkTypeError,
    MissingNotificationTitleError,
    MissingUserIDError,
    NotificationNotFoundError
} from '@src/utils/http-errors';
import { inject, injectable, } from 'inversify';
import mongoose from 'mongoose';
import { NotificationModel } from '../models/notification.model';
import { NotificationStore, Notification } from '@src/interfaces';
import { SocketsService } from '@src/services/sockets';

@injectable()
export class NotificationsService {


    @inject(SocketsService) socketsService: SocketsService;



    model = NotificationModel;



    constructor() { }



    /**
     * Creates a notification
     * @return A promise to be the created notification.
     */
    async create(
        userId: string,
        notificationType: string,
        title: string,
        description: string,
        linkType: string,
        linkData: string
    ): Promise<Notification> {

        if (!userId) { throw new MissingUserIDError(); }
        if (!title) { throw new MissingNotificationTitleError(); }
        if (!description) { throw new MissingNotificationDescriptionError(); }
        if (!notificationType) { throw new MissingUserIDError(); }
        if (!linkType) { throw new MissingNotificationLinkTypeError(); }
        if (linkType !== 'none' && !linkData) { throw new MissingNotificationLinkDataError(); }

        try {
            const notificationStore = new NotificationModel({
                userId,
                notificationType,
                title,
                description,
                linkType,
                linkData,
                timestamp: Date.now()
            });
            return this.storeToDict(await notificationStore.save());
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Creates and sends a notification
     * @return A promise to be the created notification.
     */
    async send(
        userId: string,
        notificationType: string,
        title: string,
        description: string,
        linkType: string,
        linkData: string
    ): Promise<Notification> {

        const notification = await this.create(userId, notificationType, title, description, linkType, linkData);
        await this.socketsService.emitToUser(userId, notification);
        return notification;

    }



    /**
     * Returns all notifications form a user
     * @return A promise to be a list of notifications.
     */
    async getAll(userId: string): Promise<Notification[]> {

        try {
            const notificationStores = await this.model.find({ userId });
            const notifications: Notification[] = [];
            for (const notificationStore of notificationStores) {
                notifications.push(await this.storeToDict(notificationStore));
            }
            return notifications;
        } catch {
            throw new DatabaseError();
        }

    }



    /**
     * Returns the notification
     * @return A promise to be the notification.
     */
    async findById(notificationId: string): Promise<Notification> {

        const notificationStore = await this.model.findById(notificationId);
        if (!notificationStore) { return null; }
        return await this.storeToDict(notificationStore);

    }



    /**
     * Deletes a notification
     */
    async delete(notificationId: string) {

        try {
            await this.model.findByIdAndDelete(notificationId);
        } catch { }

    }






    storeToDict(notificationStore: NotificationStore & mongoose.Document): Notification {
        if (!notificationStore) { return null; }
        return {
            type: 'notification',
            id: notificationStore.id.toString(),
            notificationType: notificationStore.notificationType,
            title: notificationStore.title,
            description: notificationStore.description,
            timestamp: notificationStore.timestamp,
            linkType: notificationStore.linkType,
            linkData: notificationStore.linkData
        };
    }
}

