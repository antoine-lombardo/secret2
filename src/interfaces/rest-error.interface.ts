export interface RestMessage {
    type: string;
}

export interface RestErrorMessage {
    type: string;
    code: string;
    description: string;
}

export interface RestSuccessMessage extends RestMessage {
    description: string;
}
