import { ObjectId } from 'mongoose';
import { PublicProfile } from './public-profile.interface';

export interface CommentStore {
    id: string;
    author: ObjectId;
    comment: string;
    hidden: boolean;
    timestamp: number;
}

export interface Comment {
    id: string;
    author: PublicProfile;
    comment: string;
    timestamp: number;
}
