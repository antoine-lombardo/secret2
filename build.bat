@ECHO OFF

SET tmp_dir=build
SET zip_file=build.zip

REM Clear temporary folder
IF EXIST %tmp_dir% RMDIR /S /Q %tmp_dir%
ROBOCOPY build_files %tmp_dir% /E

REM Clear zip file
IF EXIST %zip_file% DEL /Q %zip_file%

REM Add necessary folders and files
MD %tmp_dir%\files
MD %tmp_dir%\files\avatars
REM MD %tmp_dir%\files\drawings
REM MD %tmp_dir%\files\drawings\active
REM MD %tmp_dir%\files\drawings\done
ROBOCOPY files\avatars\base %tmp_dir%\files\avatars\base /E

REM Build
CALL npm run build
ROBOCOPY dist %tmp_dir%\dist /E
ROBOCOPY . %tmp_dir% package.json
ROBOCOPY . %tmp_dir% package-lock.json

REM Zip
cd build
call 7z a -tzip ..\%zip_file% *
cd ..

REM Clean
IF EXIST %tmp_dir% RMDIR /S /Q %tmp_dir%

pause