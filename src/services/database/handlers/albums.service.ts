import { Album, AlbumStore, PublicProfile, User } from '@src/interfaces';
import { AlbumModel, DrawingModel } from '@src/services/database/models';
import { AlbumCreatorPermissionError, AlbumNotFoundError, DatabaseError } from '@src/utils/http-errors';
import { Request } from 'express';
import { inject, injectable } from 'inversify';
import mongoose from 'mongoose';
import { UsersService } from './users.service';

import getDecorators from 'inversify-inject-decorators';
import { container } from '@src/inversify.config';
const {lazyInject} = getDecorators(container);

@injectable()
export class AlbumsService {
    @lazyInject(UsersService) usersService: UsersService;



    model = AlbumModel;



    constructor() {
        this.usersService.getUsersAlbumsNumber = async (userId: string) => {
            const albums = await this.model.find({members: userId});
            return (albums) ? albums.length : 0;
        };
    }



    /**
     * Finds the album from the provided id.
     * @param id The id of the album.
     * @return A promise to be either the album or null if not found.
     */
    async findById(id: string, userId: string, request: Request): Promise<Album> {
        try {
            const albumStore = await this.model.findById(id);
            if (!albumStore) { return null; }
            return await this.storeToDict(albumStore, userId, request);
        } catch {
            return null;
        }
    }



    /**
     * Returns the public album
     * @param request The HTTP request.
     * @return A promise to be the public album
     */
    async findPublic(request: Request): Promise<Album> {
        try {
            const albumStore = await this.model.findOne({private: false});
            return await this.storeToDict(albumStore, null, request);
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns all albums
     * @param userID The user ID of the user creating the album.
     * @param request The HTTP request.
     * @return A promise to be a list of albums.
     */
    async findAll(userId: string, request: Request): Promise<Album[]> {
        try {
            const albumStores = await this.model.find({private: true});
            const albums = [] as Album[];
            for (const albumStore of albumStores) {
                albums.push(await this.storeToDict(albumStore, userId, request));
            }
            return albums;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Returns albums from user id
     * @param fromUserId The album creator user ID.
     * @param reqUserId The user ID of the user making the request.
     * @param request The HTTP request.
     * @return A promise to be a list of albums.
     */
    async findFromUser(fromUserId: string, reqUserId: string, request: Request): Promise<Album[]> {
        try {
            const albumStores = await this.model.find({creator: fromUserId, private: true});
            const albums = [] as Album[];
            for (const albumStore of albumStores) {
                albums.push(await this.storeToDict(albumStore, reqUserId, request));
            }
            return albums;
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Adds a member to an album
     * @param albumId The album creator user ID.
     * @param memberId The album creator user ID.
     */
    async addMember(albumId: string, memberId: string) {
        try {
            await this.model.findByIdAndUpdate(albumId, { $push: { members: memberId } });
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Removes a member from an album
     * @param albumId The album creator user ID.
     * @param memberId The album creator user ID.
     */
    async removeMember(albumId: string, memberId: string) {
        try {
            await this.model.findByIdAndUpdate(albumId, { $pull: { members: memberId } });
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Creates a private album
     * @param userID The user ID of the user creating the album.
     * @return A promise to be the created album.
     */
    async create(userId: string, name: string, description: string, request: Request): Promise<Album> {
        const album = new AlbumModel({
            creator: userId,
            name,
            description,
            private: true,
            members: [userId]
        });
        try {
            return await this.storeToDict(await album.save(), userId, request);
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Modifies a private album
     * @return A promise to be the modified album.
     */
    async modify(userId: string, albumId: string, name: string, description: string, request: Request): Promise<Album> {
        const album = await this.findById(albumId, userId, request);
        if (!album) { throw new AlbumNotFoundError(); }
        const modifiedAlbum = await this.model.findByIdAndUpdate(albumId, {name, description}, {new: true});
        try {
            return await this.storeToDict(await modifiedAlbum, userId, request);
        } catch {
            throw new DatabaseError();
        }
    }



    /**
     * Deletes an album it the requesting user is the creator.
     * @param albumID The album ID to delete.
     * @param userID The requesting user ID.
     * @return A promise to be true.
     */
    async delete(userId: string, albumID: string) {
        const album = await this.model.findById(albumID);
        if (!album) { throw new AlbumNotFoundError(); }
        if (userId !== album.creator.toString()) { throw new AlbumCreatorPermissionError(); }
        try {
            await album.delete();
        } catch {
            throw new DatabaseError();
        }
    }



    async storeToDict(albumStore: AlbumStore & mongoose.Document, userId: string, request: Request): Promise<Album> {
        // Populate
        const populatedAlbumStore = await albumStore.populate(['creator', 'members']);
        // Get private profiles
        const creator = populatedAlbumStore.private ? this.usersService.getPublicProfileFromUser((populatedAlbumStore.creator as any)._doc as User, request) : null;
        const members = [] as PublicProfile[];
        let hasAccess = !populatedAlbumStore.private;
        populatedAlbumStore.members.forEach(member => {
            const dictMember = this.usersService.getPublicProfileFromUser((member as any)._doc as User, request);
            members.push(dictMember);
            if (dictMember.id === userId) { hasAccess = true; }
        });
        // Check if has exposition
        const exposedDrawings = await DrawingModel.find({album: albumStore.id, isExposed: true});
        const hasExposition = (exposedDrawings && exposedDrawings.length > 0) ? true : false;

        // Create album structure
        return {
            id: albumStore.id,
            name: albumStore.name,
            description: albumStore.description,
            private: albumStore.private,
            hasAccess,
            creator,
            members,
            hasExposition
        };
    }
}

