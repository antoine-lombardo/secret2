import { Server } from 'socket.io';

export class WSController {

    private app: Server;

    constructor(app: Server) {
        this.app = app;
    }

    initRoutes(): void {}
}
