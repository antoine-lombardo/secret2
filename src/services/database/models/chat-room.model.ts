import { ChatRoomStore } from '@src/interfaces/chat-room.interface';
import mongoose from 'mongoose';

export const ChatRoomModel = mongoose.model<ChatRoomStore>(
    'ChatRoom',
    new mongoose.Schema({
        title: String,
        creator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        isPublicRoom: Boolean,
        isDrawingRoom: Boolean,
        drawing: {type: mongoose.Schema.Types.ObjectId, ref: 'Drawing'},
        password: String,
    })
);
