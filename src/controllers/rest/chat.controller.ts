import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpDelete, httpGet, httpPatch, httpPost, request, requestParam } from 'inversify-express-utils';
import { ChatRoom } from '@src/interfaces';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { ChatService, ChatRoomsService, SocketsService } from '@src/services';
import * as HttpErrors from '@src/utils/http-errors';



@controller('/chat')
export class ChatController extends BaseHttpController {

  @inject(ChatRoomsService) public chatRoomsService: ChatRoomsService;
  @inject(ChatService) public chatService: ChatService;
  @inject(SocketsService) public socketsService: SocketsService;

  @httpGet('/public', AuthMiddleware)
  async getPublic() {
    // Get the chat room
    const chatRoom = await this.chatRoomsService.createPublicChat();
    if (!chatRoom) { throw new HttpErrors.DatabaseError(); }

    return {
      type: 'success',
      chatRoom
    };
  }

  @httpGet('/:id', AuthMiddleware)
  async getId(@requestParam('id') id: string) {
    // Get the chat room
    const chatRoom = await this.chatRoomsService.findByID(id);
    if (!chatRoom) { throw new HttpErrors.ChatRoomNotFoundError(); }

    return {
      type: 'success',
      chatRoom
    };
  }

  @httpGet('/', AuthMiddleware)
  async getAll() {
    // Get the chat room
    const chatRooms = await this.chatRoomsService.findAllPrivate();

    return {
      type: 'success',
      chatRooms
    };
  }

  @httpPost('/', AuthMiddleware)
  async create(@request() req: Request) {
    const title: string = req.body.title;
    let password: string = req.body.password;

    // Missing parameters
    if (!title) { throw new HttpErrors.MissingChatRoomTitleError(); }

    // Fix password
    if (!password) { password = null; }

    // Create the drawing
    const chatRoom = await this.chatRoomsService.createPrivateChat(this.httpContext.user.details.id, title, password);
    if (!chatRoom) { throw new HttpErrors.DatabaseError(); }

    return {
      type: 'success',
      description: 'Salle de clavardage créée.',
      chatRoom
    };

  }


  @httpPatch('/:id', AuthMiddleware)
  async modify(@requestParam('id') id: string, @request() req: Request) {
    const title: string = req.body.title;
    const passwordModified: boolean = req.body.passwordModified;
    let password: string = req.body.password;

    // Missing parameters
    if (!id) { throw new HttpErrors.MissingChatRoomTitleError(); }
    if (!title) { throw new HttpErrors.MissingChatRoomTitleError(); }
    if (passwordModified !== true && passwordModified !== false)  { throw new HttpErrors.MissingChatRoomPasswordModifiedError(); }

    // Fix password
    if (!password) { password = null; }

    // Modify the drawing
    const chatRoom = await this.chatRoomsService.modifyPrivateChat(this.httpContext.user.details.id, id, title, passwordModified, password);
    if (!chatRoom) { throw new HttpErrors.DatabaseError(); }

    return {
      type: 'success',
      description: 'Salle de clavardage modifiée.',
      chatRoom
    };

  }


  @httpDelete('/:id', AuthMiddleware)
  async delete(@requestParam('id') id: string) {
    await this.chatRoomsService.delete(this.httpContext.user.details.id, id);
    await this.chatService.handleChatDeleted(id);
    return {
      type: 'success',
      description: 'Salle de clavardage supprimée.'
    };
  }


  @httpPost('/:id/join', AuthMiddleware)
  async joinById(@requestParam('id') id: string, @request() req: Request) {
    if (id === 'public') {
      // Retreive drawing from the database
      const chatRoom = await this.chatRoomsService.createPublicChat();
      if (!chatRoom) { throw new HttpErrors.ChatRoomNotFoundError(); }
      // Join
      return this.join(chatRoom);
    } else {
      const password: string = req.body.password;
      // Retreive drawing from the database
      const chatRoom = await this.chatRoomsService.findByID(id);
      if (!chatRoom) { throw new HttpErrors.ChatRoomNotFoundError(); }
      // Check password
      if (this.httpContext.user.details.id !== chatRoom.creatorId) {
        if (chatRoom.hasPassword && !(await this.chatRoomsService.checkPassword(id, password))) {
          throw new HttpErrors.WrongChatRoomPasswordError();
        }
      }
      // Join
      return this.join(chatRoom);
    }
  }

  private async join(chatRoom: ChatRoom) {
    // Search for the user socket
    const socket = this.socketsService.getUserSocket(this.httpContext.user.details.id);
    if (!socket) { throw new HttpErrors.SocketDisconnectedError(); }
    // Get chat session
    await this.chatService.handleJoin(socket, chatRoom.id);
    // List all active users
    const activeUsers = this.chatService.getActiveUsers(chatRoom.id);
    // List messages
    const messages = this.chatService.getMessages(chatRoom.id);
    return {
      type: 'success',
      description: 'Salle de clavardage rejoint avec succès.',
      messages,
      activeUsers,
      chatRoom
    };
  }


  @httpGet('/:id/leave', AuthMiddleware)
  async get(@requestParam('id') id: string) {
    // Search for the user socket
    const socket = this.socketsService.getUserSocket(this.httpContext.user.details.id);
    if (!socket) { throw new HttpErrors.SocketDisconnectedError(); }
    // Remove user from chat session
    await this.chatService.handleQuit(socket, id);
    return {
      type: 'success',
      description: 'Salle de clavardage quittée avec succès.'
    };
  }

}
