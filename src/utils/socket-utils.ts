import { Socket } from 'socket.io';



export function formatIp(socket: Socket): string {
    if (socket.handshake.headers['x-forwarded-for']) {
        return socket.handshake.headers['x-forwarded-for'] as string;
    }
    else {
        return socket.handshake.address.substring(socket.handshake.address.lastIndexOf(':') + 1);
    }
}



export function formatIpPrefix(socket: Socket): string {
    const ip = formatIp(socket);
    return (ip) ? `${ip} -> ` : '';
}
