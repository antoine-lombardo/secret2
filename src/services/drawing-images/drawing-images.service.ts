import { injectable } from 'inversify';
import { config } from '@src/config';
import * as fs from 'fs';
import * as path from 'path';
import { Request } from 'express';
import { formatHostname } from '@src/utils/rest-utils';
import moment from 'moment';
import { InternalError, InvalidAvatarTypeError, InvalidImageDataError } from '@src/utils/http-errors';
import sharp from 'sharp';
import { DrawingImage } from '@src/interfaces/drawing-image.interface';

const DRAWINGS_PATH = path.normalize(config.FilesDir + '/drawings');
const THUMBNAIL_WIDTH  = 300;
const THUMBNAIL_HEIGHT = 200;

@injectable()
export class DrawingImagesService {

    constructor() {}

    getDirs(type: string, id: string): DrawingImage | null {
        // Image type
        let basePath: string;
        switch (type) {
            case 'active':
                basePath = '/active';
                break;
            case 'done':
                basePath = '/done';
                break;
            default:
                throw new InternalError();
        }
        if (!fs.existsSync(`${DRAWINGS_PATH}${basePath}/${id}`)) { return null; }
        const images = fs.readdirSync(`${DRAWINGS_PATH}${basePath}/${id}`).sort().reverse();
        for (const image of images) {
            if (
                fs.existsSync(`${DRAWINGS_PATH}${basePath}/${id}/${image}/original.png`) &&
                fs.existsSync(`${DRAWINGS_PATH}${basePath}/${id}/${image}/thumbnail.png`)
            ) {
                return {
                    original: `${basePath}/${id}/${image}/original.png`,
                    thumbnail: `${basePath}/${id}/${image}/thumbnail.png`
                };
            }
        }
        return null;
    }



    getUrls(type: string, id: string, req: Request): DrawingImage | null {
        const dir = this.getDirs(type, id);
        if (!dir) { return null; }
        return {
            original: `${formatHostname(req)}/files/drawings${dir.original}`,
            thumbnail: `${formatHostname(req)}/files/drawings${dir.thumbnail}`
        };
    }



    async save(type: string, id: string, dataUrl: string): Promise<DrawingImage> {
        // Image type
        let basePath: string;
        switch (type) {
            case 'active':
                basePath = '/active';
                break;
            case 'done':
                basePath = '/done';
                break;
            default:
                throw new InternalError();
        }

        // Parse data url
        const regex = /^data:.+\/(.+);base64,(.*)$/;
        dataUrl = dataUrl.replace(/(\r\n|\n|\r)/gm, '');
        const matches = dataUrl.match(regex);
        if (!matches || matches.length < 2) { throw new InvalidImageDataError(); }
        const ext = matches[1];
        const data = matches[2];
        if (ext !== 'png') { throw new InvalidAvatarTypeError(); }

        // Make directory
        const drawingPath = `${basePath}/${id}`;
        if (!fs.existsSync(`${DRAWINGS_PATH}${drawingPath}`)) { fs.mkdirSync(`${DRAWINGS_PATH}${drawingPath}`); }
        const imagePath = `${drawingPath}/${moment().format('YYYYMMDDHHmmss')}`;
        if (!fs.existsSync(`${DRAWINGS_PATH}${imagePath}`)) { fs.mkdirSync(`${DRAWINGS_PATH}${imagePath}`); }
        const originalPath = `${imagePath}/original.png`;
        const thumbnailPath = `${imagePath}/thumbnail.png`;
        if (fs.existsSync(`${DRAWINGS_PATH}${originalPath}`)) { fs.unlinkSync(`${DRAWINGS_PATH}${originalPath}`); }
        if (fs.existsSync(`${DRAWINGS_PATH}${thumbnailPath}`)) { fs.unlinkSync(`${DRAWINGS_PATH}${thumbnailPath}`); }

        // Save original
        const buffer = Buffer.from(data, 'base64');
        fs.writeFileSync(`${DRAWINGS_PATH}${originalPath}`, buffer);
        if (!fs.existsSync(`${DRAWINGS_PATH}${originalPath}`)) { throw new InternalError(); }

        // Make resized
        await this.resize(`${DRAWINGS_PATH}${originalPath}`, `${DRAWINGS_PATH}${thumbnailPath}`, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
        if (!fs.existsSync(`${DRAWINGS_PATH}${originalPath}`)) { throw new InternalError(); }

        // Return paths
        return {
            original: originalPath,
            thumbnail: thumbnailPath
        };


    }

    private async resize(input: string, output: string, width: number, height: number) {
    if (fs.existsSync(output)) { fs.unlinkSync(output); }
    await sharp(input)
      .resize(width, height)
      .png()
      .toFile(output);
  }

}
