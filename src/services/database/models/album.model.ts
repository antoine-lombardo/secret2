import { AlbumStore } from '@src/interfaces';
import mongoose from 'mongoose';

export const AlbumModel = mongoose.model<AlbumStore>(
    'Album',
    new mongoose.Schema({
        name: String,
        creator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        description: String,
        private: Boolean,
        members: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
    })
);
