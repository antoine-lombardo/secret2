export interface PresetAvatar {
    name: string;
    original: string;
    resized: string;
}
