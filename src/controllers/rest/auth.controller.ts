
import { Request, Response } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpGet, httpPost, request, response } from 'inversify-express-utils';
import { logger } from '@services/logger';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { AuthenticationService, UsersService, SocketsService } from '@src/services';
import { formatIpPrefix } from '@src/utils/rest-utils';
import * as HttpErrors from '@src/utils/http-errors';

@controller('/auth')
export class AuthController extends BaseHttpController {



  @inject(SocketsService) private socketsService: SocketsService;
  @inject(UsersService) public usersService: UsersService;
  @inject(AuthenticationService) public authenticationService: AuthenticationService;



  @httpPost('/login')
  async login(@request() req: Request) {
    // Parameters
    let email: string = req.body.email;
    const password: string = req.body.password;

    // Missing parameters
    if (!email || !(email = email.trim()) || !password) {
      throw new HttpErrors.MissingEmailError();
    }

    const user = await this.usersService.findUserByEmail(email);

    // Username does not exist
    if (!user) {
      throw new HttpErrors.EmailNotFoundError();
    }

    // User already logged in
    if (await this.socketsService.userConnected(user)) {
      throw new HttpErrors.AlreadyLoggedInError();
    }

    // Invalid password
    if (!this.authenticationService.checkPassword(user, password)) {
     throw new HttpErrors.InvalidPasswordError();
    }

    // Generate token
    const token = await this.authenticationService.generateToken(user);
    // Get private profile
    const privateProfile = await this.usersService.getPrivateProfile(user.id, req);
    // Handle login
    await this.usersService.handleLogin(user.id);
    logger.debug(`[REST] ${formatIpPrefix(req)}${user.username}: Logged in`);
    return {
      type: 'success',
      description: 'Connexion complétée.',
      authInfos: {
        id: user.id,
        username: user.username,
        email: user.email,
        accessToken: token
      },
      privateProfile
    };

  }



  @httpPost('/signup')
  async signup(@request() req: Request, @response() res: Response) {
    const username: string = req.body.username;
    const password: string = req.body.password;
    const email: string = req.body.email;
    let darkMode: boolean = req.body.darkMode;
    const avatarType: string = req.body.avatarType;
    const avatarValue: string = req.body.avatarValue;

    // Wrong avatar parameter
    if (darkMode !== true && darkMode !== false) { darkMode = false; }
    if (!avatarValue) { throw new HttpErrors.MissingAvatarError(); }

    // Wrong avatar parameter
    if (avatarType !== 'upload' && avatarType !== 'preset') { throw new HttpErrors.UnknownAvatarTypeError(); }
    if (!avatarValue) { throw new HttpErrors.MissingAvatarError(); }

    // Missing parameters
    if (!username || !password || !email) {
      throw new HttpErrors.MissingFieldsError();
    }

    // Check for duplicates
    if (await this.usersService.usernameExists(username)) {
      throw new HttpErrors.UsernameAlreadyExistError();
    }
    else if (await this.usersService.emailExists(email)) {
      throw new HttpErrors.EmailAlreadyUsedError();
    }

    // Try to create user
    await this.usersService.createUser(username, email, password, darkMode, avatarType, avatarValue);

    return {
      type: 'success',
      description: 'Compte créé.'
    };

  }



  @httpGet('/verifyToken', AuthMiddleware)
  async verifyToken() {
    return {
      type: 'success',
      description: 'Le jeton est valide.',
      authInfos: {
        id: this.httpContext.user.details.id,
        username: this.httpContext.user.details.username,
        email: this.httpContext.user.details.email
      }
    };
  }
}
