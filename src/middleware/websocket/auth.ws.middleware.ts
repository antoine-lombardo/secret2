import { AuthenticatedSocket } from '@src/interfaces';
import { AlreadyLoggedInError, InvalidAccessTokenError, MissingAccessTokenError } from '@src/utils/http-errors';
import { BaseWSMiddleware } from './base.ws.middleware';
import { logger } from '@src/services/logger';
import { formatIpPrefix } from '@src/utils/socket-utils';
import { inject, injectable, LazyServiceIdentifer } from 'inversify';
import { AuthenticationService, UsersService, SocketsService } from '@src/services';

@injectable()
export class AuthWSMiddleware extends BaseWSMiddleware {

    private socketsService: SocketsService;
    private authenticationService: AuthenticationService;
    private usersService: UsersService;

    constructor(
        @inject(new LazyServiceIdentifer(() => SocketsService)) socketsService: SocketsService,
        @inject(new LazyServiceIdentifer(() => AuthenticationService)) authenticationService: AuthenticationService,
        @inject(new LazyServiceIdentifer(() => UsersService)) usersService: UsersService
    ) {
        super();
        this.socketsService = socketsService;
        this.authenticationService = authenticationService;
        this.usersService = usersService;
    }

    async middleware(socket: AuthenticatedSocket, next: (err?: Error) => void): Promise<void> {
        socket.authFailed = false;
        const token = socket.handshake.query.token as string;
        if (!token) {
            this.socketsService.sendError(new MissingAccessTokenError(), next);
            socket.authFailed = true;
            return;
        }
        const payload = this.authenticationService.validateToken(token);
        if (!payload) {
        this.socketsService.sendError(new InvalidAccessTokenError(), next);
            socket.authFailed = true;
            return;
        }
        const user = await this.usersService.findUserById(payload.id);
        if (!user) {
            this.socketsService.sendError(new InvalidAccessTokenError(), next);
            socket.authFailed = true;
            return;
        }

        socket.authFailed = await this.socketsService.userConnected(user);

        if (socket.authFailed) {
            this.socketsService.sendError(new AlreadyLoggedInError(), next);
            return;
        }
        socket.user = user;
        logger.debug(`[SOCKET] ${formatIpPrefix(socket)}${user.username}: Connected`);
        next();
    }
}
