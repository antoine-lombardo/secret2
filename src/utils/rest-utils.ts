import { Request } from 'express';



export function formatIp(request: Request): string {
    if (request.headers['x-forwarded-for']) {
        return request.headers['x-forwarded-for'] as string;
    }
    else {
        try {
            const ip = request.ip.substring(request.ip.lastIndexOf(':') + 1);
            return (ip === '1') ? '127.0.0.1' : ip;
        } catch {
            return null;
        }
    }
}



export function formatHostname(request: Request): string {
    let host = request.headers['x-forwarded-host'] as string;
    host = host ? host : request.headers.host;
    let protocol = request.headers['x-forwarded-proto'] as string;
    protocol = protocol ? protocol : request.protocol;
    return protocol + '://' + host;
}



export function formatIpPrefix(request: Request): string {
    const ip = formatIp(request);
    return (ip) ? `${ip} -> ` : '';
}



export function retreiveAccessTokenFromHeaders(request: Request): string {
    const token = request.headers['authorization'] as string;
    if (!token) { return null; }
    return token.replace('Bearer ', '');
}
