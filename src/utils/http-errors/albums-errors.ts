import { HttpError } from './error-base';
import { StatusCodes } from 'http-status-codes';

// Missing Fields

export class MissingAlbumNameError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_ALBUM_NAME', 'Nom de l\'album manquant.');
    }
}

export class MissingAlbumDescriptionError extends HttpError {
    constructor() {
        super(StatusCodes.BAD_REQUEST, 'MISSING_ALBUM_DESCRIPTION', 'Description de l\'album manquante.');
    }
}

// Not found

export class AlbumNotFoundError extends HttpError {
    constructor() {
        super(StatusCodes.NOT_FOUND, 'ALBUM_NOT_FOUND', 'Impossible de trouver l\'album.');
    }
}

// Permission

export class AlbumCreatorPermissionError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'ALBUM_CREATOR_PERMISSION', 'Vous devez être le créateur de l\' album pour faire cela.');
    }
}

export class AlbumMemberPermissionError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'ALBUM_MEMBER_PERMISSION', 'Vous devez être membre de l\' album pour faire cela.');
    }
}

export class AlbumCreatorCannotQuitError extends HttpError {
    constructor() {
        super(StatusCodes.UNAUTHORIZED, 'ALBUM_CREATOR_CANNOT_QUIT', 'Le créateur ne peut pas quitter son album.');
    }
}
