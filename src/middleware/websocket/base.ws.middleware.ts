import { AuthenticatedSocket } from '@src/interfaces';
import { injectable } from 'inversify';

@injectable()
export abstract class BaseWSMiddleware {

    async middleware(socket: AuthenticatedSocket, next: (err?: Error) => void): Promise<void> {
        next();
    }
}
