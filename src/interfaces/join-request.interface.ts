import { ObjectId } from 'mongoose';
import { Album } from './album.interface';
import { PublicProfile } from './public-profile.interface';

export interface JoinRequestStore {
    _id: ObjectId;
    id: string;
    album: ObjectId;
    user: ObjectId;
    dateCreated: Date;
    dateAnswered: Date;
    accepted: boolean;
}

export interface JoinRequest {
    id: string;
    album: Album;
    user: PublicProfile;
    dateCreated: number;
    dateAnswered: number;
    answered: boolean;
    accepted: boolean;
}
