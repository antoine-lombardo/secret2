import 'module-alias/register';
import 'reflect-metadata';
import { container } from './inversify.config';
import { initLogger } from '@services/logger';

initLogger();

import { ChatService, CollabService, ServerService } from './services';

// run root service
container.get<ServerService>(ServerService);
// run top services
container.get<ChatService>(ChatService).loadPublicChatSession();
container.get<CollabService>(CollabService);
