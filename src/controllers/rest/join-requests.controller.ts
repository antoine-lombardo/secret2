import { Request } from 'express';
import { inject } from 'inversify';
import { BaseHttpController, controller, httpPost, request, requestParam } from 'inversify-express-utils';
import { AuthMiddleware } from '@src/middleware/rest/auth.middleware';
import { JoinRequestsService, NotificationsService } from '@src/services';



@controller('/joinRequests')
export class JoinRequestsController extends BaseHttpController {


  @inject(JoinRequestsService) public joinRequestsService: JoinRequestsService;
  @inject(NotificationsService) public notificationsService: NotificationsService;



  @httpPost('/:requestId/accept', AuthMiddleware)
  async acceptRequest(@requestParam('requestId') requestId: string, @request() req: Request) {
    const joinRequest = await this.joinRequestsService.findById(requestId, this.httpContext.user.details.id, req);
    await this.joinRequestsService.answer(requestId, true, this.httpContext.user.details.id, req);
    this.notificationsService.send(
      joinRequest.user.id,
      'access_granted',
      'Demande d\'accès approuvée',
      `Votre demande d\'accès à l\'album "${joinRequest.album.name}" a été approuvée.`,
      'album',
      joinRequest.album.id
    );
    return {
      type: 'success',
      description: 'Demande acceptée.'
    };
  }

  @httpPost('/:requestId/decline', AuthMiddleware)
  async declineRequest(@requestParam('requestId') requestId: string, @request() req: Request) {
    const joinRequest = await this.joinRequestsService.findById(requestId, this.httpContext.user.details.id, req);
    await this.joinRequestsService.answer(requestId, false, this.httpContext.user.details.id, req);
    this.notificationsService.send(
      joinRequest.user.id,
      'access_denied',
      'Demande d\'accès refusée',
      `Votre demande d\'accès à l\'album "${joinRequest.album.name}" a été refusée.`,
      'none',
      null
    );
    return {
      type: 'success',
      description: 'Demande refusée.'
    };
  }



}
